# pyTimeSched

pyTimeSched is a simple and lightweight time tracking tool.
You can track elapsed time for tasks and projects and use the data for the recording of time worked.

Uses monospaced font from JetBrains for best readability (Apache 2.0 Licence).

Releases can be found in [Releases](https://gitlab.com/ilmenshik/pytimesched/-/releases)

Changelist can be found in [CHANGELOG.md](CHANGELOG.md)

## Documentation
### Overview

![Main window](screenshot.png?raw=true "Main window")

### Features
- [x] Light and dark themes
- [x] Export via RESTful services
- [x] Export to CSV (file)
- [x] Only one instance: first instance will be restored and focused with second
- [x] Friendly GUI
- [x] Memorize and restores last app position and size
- [x] A lot of hotkeys

### Requirements (dev)
* Python 3.8+
* Python pip
* Python modules (see requirements-3.8.txt and requirements-3.10.txt)
* Python virtual environment

### How to start
* Unzip distributive
* Launch:
    * Windows: pyTimeSched.exe
    * Linux: ./pyTimeSched

### Settings
* Use dark theme - change windows color theme
* Popup to mouse - center window to mouse pointer position when it launch (can be usefull with shortcut for launch app)
* Reset today after midnight - reset today column for all projects after 00:00
* Configure REST connection - configure url, login and token for export via RESTful services
* Export via REST - use RESTful for export instead of CSV (file)

### Projects control
* Add project:
    * Projects -> Add
    * Shortcut: Insert
    * Button "Add" in main window 
* Change project title: double click on title column 
* Remove selected projects: Projects -> Remove (shortcut: Delete) 
* Start/pause selected project: Projects -> Start/Pause (shortcut: Space)
* Change project color mark:
    * Click third column in project row
    * Choose color
    * Press "Select" button
* Edit project creation date: double click on created column\
  Possible format: YYYY-MM-DD\
  Wrong format will be ignored
* Edit project time: double click on time columns\
  Can be edited only when project is paused\
  Possible format: [Time overall and today](#time-overall-and-today)\
  Wrong format will be ignored
* Mark/unmark project as done: press on checkbox in first column in project row


#### Time overall and today

Supported format: OHSM
```
O - Operator (optional):
    "+" to add;
    "-" to substract;
    Nothing to set the exact time.

H - Hours, optional if means zero.

S - Separator (optional):
    ":", ".." for normal format (0:15 = 15 minutes);
    "." for decimal format (0.75 = 45 minutes);
    Can be used commas instead of dots.
    

M - Minutes in normal format or decimal part of hour.
    Optional, works only with separator (S).
    
```
Examples:
* Set hour and a half: 1.5 or 1:30
* Reduce by 10 minutes: -0.17 or -:10
* Add 15 minutes: +.25 or +..15

### Export projects to CSV
Generate CSV with format "PROJECT_TITLE;HH:MM:SS", shows it for copy-paste and saves to file into "export" directory

### Export projects via RESTfull
* Open: Projects -> Settings -> Configure REST connection
* Set API type, URL, login and token
* Be sure about checked menu Projects -> Settings -> Export via REST
* Use Export submenues under menu Action or Export buttons on then main window
* Supported formats:
  * Simple: JSON list of objects kind {project: title, hours: 0.0, date: 2022-02-24}
  * Jira (v2): [Worklog API Docs](https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issue-worklogs/#api-rest-api-2-issue-issueidorkey-worklog-post)

### Reset project time
Reset - means reset today column for selected or all projects.\
Reset will not reduce total column.\
Autoreset will be executed at midnight if option selected in settings.

### Templates
Use for instant add project (everyday routine)\
After edit template list can be called by shortcut Ctrl+N (N - number of template)

### Search
Highlight projects which match inputted pattern\
Shortcut: Ctrl+F\
Esc: clear search field 

### Status bar
Shows data:
* Total projects in table
* Sum overall time
* Sum today time
* Left time to work (with standard 8 work hours)

If selected more than one project in table, data will be limited with only them. 

### Logs
Short log available from GUI after pressing button "Show log".

Short log can be cleared by pressing button "Clear log", when log is visible.

Full work logs can be found into "logs" directory. Files are rotating every day, saves only last 5.


## Building from source

### Prepare
Linux:
```bash
python3.10 -m venv venv
source venv/bin/activate
pip install -r requirements.txt -r requirements.dev.txt
```
Windows:
```
python -m venv venv
venv\Scripts\activate
pip install -r requirements.txt -r requirements.dev.txt
```

### Run
```bash
python main.py
```

### Build binary
Update app icon for Windows:
```
python -c "from PIL import Image; Image.open(r'gui/icons/icon.png').save('gui/icons/icon.ico', sizes=[(16, 16), (32, 32), (48, 48), (64, 64)])"
```

Compile distributive:
* Linux: `./compile.sh`
* Windows: `compile.bat`

Binary will be compiled into "distr" folder.

Required additional folders:
* gui/fonts
* gui/themes
* gui/icons


### Edit GUI
Only basics, without any data.

Do not edit files in `gui/windows/` and `gui/src/` manually.

```bash
# Edit source file
pyqt5designer gui/src/main.ui

# Convert UI (xml) to python format
pyuic5 gui/src/main.ui -o gui/windows/main.py
```

