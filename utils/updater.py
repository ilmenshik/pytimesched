import logging
from re import compile
from json import loads
from requests import get
from distutils.version import LooseVersion
from utils.settings import settings, REQUEST_TIMEOUTS


UPDATE_URL = 'https://gitlab.com/api/v4/projects/ilmenshik%2Fpytimesched/repository/tags'
UPDATE_RELEASES_URL = 'https://gitlab.com/ilmenshik/pytimesched/-/releases/{version}'
REGEX_VALID_VERSION = compile(r'^v[0-9.]+.*')
REGEX_VERSION_EXTRACTOR = compile(r'^(v[0-9.]+)?.*')


def get_release_url(version):
    return UPDATE_RELEASES_URL.format(version=version)


def get_newer_tag(current):
    if not current or not REGEX_VALID_VERSION.match(current):
        current = 'v0.0'

    current_version = LooseVersion(extract_version(str(current)))
    max_tag = max(get_releases_tags() or [None])
    return str(max_tag) if max_tag > current_version else None


def extract_version(tag):
    return REGEX_VERSION_EXTRACTOR.sub('\\1', tag) or 'v0.0'


def get_releases_tags():
    try:
        data = loads(get(UPDATE_URL, timeout=REQUEST_TIMEOUTS).text)
        return [LooseVersion(extract_version(tag.get('name'))) for tag in data if
                REGEX_VALID_VERSION.match(tag.get('name'))]
    except Exception as e:
        if settings.logger.level == logging.DEBUG:
            settings.logger.exception(e)

        return []
