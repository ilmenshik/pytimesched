import tarfile
from os.path import basename, join
from sys import argv


def exclude_pycache(tar_info: tarfile.TarInfo):
    return tar_info if basename(tar_info.name) != '__pycache__' else None


def make_archive(archive, path_list):
    tar = tarfile.open(archive, 'w:gz')

    try:
        for path, path_prefix in path_list:
            if basename(path) == '__pycache__':
                continue

            target_path = path
            if path_prefix is not None:
                target_path = join(path_prefix, basename(path))

            print('Add', path, 'at', target_path, 'into', archive)
            tar.add(path, arcname=target_path, filter=exclude_pycache)
    finally:
        tar.close()


archive_name = argv[1]
make_archive(archive_name, [
    ['distr/pyTimeSched', ''],
    ['gui/icons', 'gui'],
    ['gui/fonts', 'gui'],
    ['gui/themes', 'gui'],
    ['README.md', ''],
    ['LICENSE.txt', ''],
])