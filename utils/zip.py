from zipfile import ZipFile, ZIP_DEFLATED
from os.path import isdir, basename, dirname, join
from os import listdir
from sys import argv
from platform import system


def make_archive(archive, path, path_prefix=None, mode_add=False, ignore_cache=True):
    if ignore_cache and basename(path) == '__pycache__':
        return

    arch = ZipFile(archive, 'w' if not mode_add else 'a', ZIP_DEFLATED)
    target = join(path_prefix or dirname(path), basename(path))

    print("Pack '{}'".format(path))
    arch.write(path, target)
    arch.close()
    if isdir(path):
        rel_path = join(path_prefix or dirname(path), basename(path))
        for file in listdir(path):
            make_archive(archive, join(path, file), rel_path, mode_add=True)


archive_name = argv[1]
target_root_dir = 'pyTimeSched'
target_gui_dir = '{}/gui'.format(target_root_dir)
binary_file = 'distr/pyTimeSched{}'.format('.exe' if system() == 'Windows' else '')

make_archive(archive_name, binary_file, target_root_dir)
make_archive(archive_name, 'README.md', target_root_dir, mode_add=True)
make_archive(archive_name, 'LICENSE.txt', target_root_dir, mode_add=True)
make_archive(archive_name, 'gui/icons', target_gui_dir, mode_add=True)
make_archive(archive_name, 'gui/fonts', target_gui_dir, mode_add=True)
make_archive(archive_name, 'gui/themes', target_gui_dir, mode_add=True)
