from re import compile
from click import command, option
import pyinstaller_versionfile as v

ALL_EXCEPT_VERSION = compile(r'[^0-9.]')


@command()
@option('-i', '--input', 'input_file', default='metadata.yml', metavar='SOURCE_FILE',
        help='Source yaml file')
@option('-o', '--output', 'output_file', default='version_file.txt', metavar='DESTINATION_FILE',
        help='Destination pyinstaller version file')
@option('-v', '--version', default='0.0.0.0', metavar='VERSION',
        help='Version (only dots and numbers)')
def main(input_file, output_file, version):
    ver = ALL_EXCEPT_VERSION.sub('', version) or '0'
    print("Version file: {i} ({s}) -> {o} ({v})".format(s=version, v=ver, i=input_file, o=output_file))
    v.create_versionfile_from_input_file(
        input_file=input_file,
        output_file=output_file,
        version=ver
    )


if __name__ == '__main__':
    main()
