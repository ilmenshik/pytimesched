import logging
from logging.handlers import TimedRotatingFileHandler
import sys
from os import getcwd
from tempfile import gettempdir
from os.path import join, getmtime, isfile, dirname
from json import dump, load, JSONDecodeError
from shutil import move
from pathlib import Path
from datetime import datetime

FORMAT_LOG = '%(asctime)s %(name)s [%(levelname)s] %(message)s'

if getattr(sys, 'frozen', False):
    BASE_DIR = dirname(sys.executable)
else:
    BASE_DIR = getcwd()

DATA_BASE_DIR = gettempdir() if '--use-temp-dir' in sys.argv else BASE_DIR

# System dirs
DIR_IMAGE = join(BASE_DIR, 'img')
DIR_STYLES = join(BASE_DIR, 'gui', 'css')
DIR_ICONS = join(BASE_DIR, 'gui', 'icons')
DIR_THEMES = join(BASE_DIR, 'gui', 'themes')

# Data dirs
DIR_DATA = join(DATA_BASE_DIR, 'data')
DIR_LOGS = join(DATA_BASE_DIR, 'logs')
DIR_EXPORT = join(DATA_BASE_DIR, 'export')

# Relative dirs
FILE_LOG = join(DIR_LOGS, 'pyTimeSched.log')
FILE_SETTINGS = join(DIR_DATA, 'settings.json')
FILE_PROJECTS = join(DIR_DATA, 'projects.json')
FILE_GEOMETRY = join(DIR_DATA, 'geometry.dat')

FONT_FAMILY = ['JetBrains Mono', 'Ubuntu', 'Hack', 'Consolas', 'Tahoma']
FILES_FONT = [
    join('gui', 'fonts', 'JetBrainsMono-Bold.ttf'),
    join('gui', 'fonts', 'JetBrainsMono-Medium.ttf'),
    join('gui', 'fonts', 'JetBrainsMono-Regular.ttf'),
]
THEME_FILE_EXT = '.thm'
THEME_DEFAULT = """
# Main light theme (Base theme: Fusion)

Window                    #efefef
WindowText                #484848
Foreground                #000000
Base                      #ffffff
AlternateBase             #f7f7f7
ToolTipBase               #ffffdc
ToolTipText               #000000
Text                      #000000
Button                    #efefef
ButtonText                #000000
BrightText                #ff0000
Link                      #2a82da
Highlight                 #ffe97f
HighlightedText           #000000

Disabled.Base             #efefef
Disabled.AlternateBase    #efefef
Disabled.Text             #bebebe
Disabled.Highlight        #999999
Disabled.HighlightedText  #ffffff
"""

DEFAULT_SETTINGS = {
    "activity_checks": False,
    "auto_check_update": True,
    "lock_sort": None,
    "popup_to_mouse": False,
    "reset_after_midnight": True,
    "rest_use": False,
    "rest_type": None,
    "rest_url": None,
    "rest_login": None,
    "rest_token": None,
    "sort_by": None,
    "show_log": False,
    "templates": [],
    "use_theme": 'default',
    "workhours": 8,
}

REST_TYPES = {
    None: '<disabled>',
    'simple': 'Simple REST API',
    'jira_v2': 'Atlassian Jira API v2',
}
REQUEST_TIMEOUTS = (5, 120)  # requests, Tuple(Connection, Read), seconds
UPDATE_INTERVAL_SEC = 60 * 60 * 4


def init_work_dirs():
    for d in [DIR_DATA, DIR_LOGS]:
        Path(d).mkdir(parents=True, exist_ok=True)


class LazySettings(object):
    _filename: str = None
    logger = None
    _debug_enabled: bool = False

    def __init__(self, d: dict, is_root: bool = True):
        self._extend(d)
        if is_root:
            init_work_dirs()
            self._debug_enabled = '--debug' in sys.argv or '-d' in sys.argv
            self._init_logger(
                logger_name='pytimesched',
                level='DEBUG' if self._debug_enabled else 'INFO',
                base_level='DEBUG' if self._debug_enabled else 'ERROR',
                filename=FILE_LOG
            )
            self._filename = FILE_SETTINGS
            self.load()

    def __str__(self):
        return str(self.serialize())

    def _extend(self, d: dict):
        for k, v in d.items():
            if isinstance(v, (list, tuple)):
                setattr(self, k, [LazySettings(x, False) if isinstance(x, dict) else x for x in v])
            else:
                setattr(self, k, LazySettings(v, False) if isinstance(v, dict) else v)

    def _init_logger(self, logger_name: str = '', level: str = 'INFO', base_level: str = 'ERROR', filename: str = None):
        logging.basicConfig(level=getattr(logging, base_level), format=FORMAT_LOG)
        self.logger = logging.getLogger(logger_name)
        self.logger.propagate = False
        self.logger.setLevel(getattr(logging, level))
        formatter = logging.Formatter(FORMAT_LOG)

        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        if filename:
            self._rotate_log(filename)
            fh = TimedRotatingFileHandler(filename, when='D', interval=1, backupCount=5)
            fh.setFormatter(formatter)
            self.logger.addHandler(fh)

    @staticmethod
    def _rotate_log(filename):
        if not isfile(filename):
            return
        last_edit = datetime.fromtimestamp(getmtime(filename)).strftime("%Y-%m-%d")
        now = datetime.now().strftime("%Y-%m-%d")
        new_name = "{}.{}".format(filename, last_edit)
        if last_edit != now and not isfile(new_name):
            move(filename, new_name)

    def serialize(self):
        serialized = dict()
        for setting in dir(self):
            attr = getattr(self, setting)
            if setting.startswith('_') or setting == 'logger' or callable(attr):
                continue
            if isinstance(attr, LazySettings):
                serialized[setting] = attr.serialize()
            elif isinstance(attr, (list, tuple)):
                serialized[setting] = [x.serialize() if isinstance(x, LazySettings) else x for x in attr]
            else:
                serialized[setting] = attr
        return serialized

    def save(self):
        tmp_buff = self._filename + '.tmp'
        if self._filename:
            dump(self.serialize(), open(tmp_buff, 'w'), indent=4)
            move(tmp_buff, self._filename)
            self.logger.debug("Saved settings to '{}'".format(self._filename))
        else:
            self.logger.error("Settings file not defined")

    def load(self):
        if not self._filename:
            self.logger.error("Settings file not defined")
            return
        try:
            self._extend(load(open(self._filename, 'r')))
            self.logger.debug("Loaded settings from '{}'".format(self._filename))
        except FileNotFoundError:
            pass
        except JSONDecodeError as e:
            self.logger.exception("Fail to parse settings file: {}".format(e))


settings = LazySettings(DEFAULT_SETTINGS)
