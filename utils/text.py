from re import compile
from datetime import datetime

REGEX_ZERO_DATE = compile(r'^(|0+|0000-00-00)$')
REGEX_VALID_DATE = compile(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}$')
REGEX_VALID_TIME = compile(r'([+-])?([0-9]*)((\.[0-9]+)|(:([0-5]?[0-9])?)(:([0-5]?[0-9])?)?)?$')


def now_format() -> str:
    return datetime.now().strftime("%Y-%m-%d %H:%M")


def generate_export_filename(prefix: str = None) -> str:
    return datetime.now().strftime("export{}-%Y-%m-%d-%H%M%S-%f.csv".format("-{}".format(prefix) if prefix else ''))


def parse_time(str_val: str) -> int:
    if not str_val or not isinstance(str_val, str):
        return 0

    str_val = str_val.replace(',', '.').replace('..', ':')
    matches = REGEX_VALID_TIME.match(str_val)

    if not matches:
        return 0

    str_val = str_val.replace('+', '').replace('-', '')

    while str_val and str_val[0] == '0':
        str_val = str_val[1:]

    hours = int(matches.group(2) or 0) * 3600
    seconds = int(matches.group(8) or 0)
    if ':' in str_val:
        minutes = int(matches.group(6) or 0) * 60
    else:
        minutes = int(round(float(matches.group(4) or 0), 2) * 60 * 60)

    return seconds + minutes + hours


def format_time(s: int):
    if not s:
        return '00:00:00'

    h = s // (60**2)
    s -= h * (60**2)
    m = s // 60
    s -= m * 60
    hh = "{}{}".format('0' if h < 10 else '', h)
    mm = "{}{}".format('0' if m < 10 else '', m)
    ss = "{}{}".format('0' if s < 10 else '', s)
    return "{}:{}:{}".format(hh, mm, ss)


def zero_or_more(var: int):
    return var if var and var > 0 else 0
