import logging
import zmq
import atexit


class OneInstanceException(Exception):
    pass


class OneInstance:
    host: str = None
    port: int = None
    context = None
    socket = None
    err: str = None
    logger = None

    def __init__(self, host='127.1.1.1', port=35513, failover_data='focus'):
        self.logger = logging.getLogger('OneInstance')
        self.host = host
        self.port = port
        self.context = zmq.Context()
        self.listen()
        if not self.socket:
            if not failover_data:
                raise OneInstanceException(self.err or 'Something wrong')
            self.send(failover_data)
            exit()
        else:
            self.logger.debug('Server tcp://{}:{}'.format(self.host, self.port))

    def is_valid(self):
        return bool(self.socket)

    def listen(self):
        self.socket = self.context.socket(zmq.REP)
        try:
            self.socket.bind('tcp://{}:{}'.format(self.host, self.port))
            atexit.register(self.cleanup)
        except zmq.error.ZMQError as e:
            logging.debug('ZMQ Error {}:'.format(e.errno), e.strerror)
            self.socket = None
            if e.errno == 98:
                self.err = e.strerror
                return

    def read(self):
        if not self.socket:
            return ''
        # msg = self.socket.recv()
        # self.socket.send(b'ok')
        # return msg.decode()
        return ''

    def send(self, msg):
        self.socket = self.context.socket(zmq.REQ)
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.connect("tcp://{}:{}".format(self.host, self.port))
        self.socket.send(msg.encode())

        poller = zmq.Poller()
        poller.register(self.socket, zmq.POLLIN)
        poller.poll(1000)  # timeout in ms

    def cleanup(self):
        self.logger.debug('Cleanup')
        if self.socket:
            self.socket.close()
        if self.context:
            self.context.term()

    def __del__(self):
        self.cleanup()
