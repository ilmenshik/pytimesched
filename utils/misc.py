from PyQt5.QtCore import Qt

DATA_ROLE = Qt.UserRole

FLAGS_SELECTABLE = Qt.ItemIsEnabled | Qt.ItemIsSelectable
FLAGS_EDITABLE = Qt.ItemIsEditable | FLAGS_SELECTABLE
FLAGS_DISABLE = Qt.NoItemFlags
