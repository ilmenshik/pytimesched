import logging
from requests import request
from json import loads, dumps
from json.decoder import JSONDecodeError
from utils.settings import settings, REQUEST_TIMEOUTS
from re import compile

JIRA_API_V2 = 'rest/api/2'
JIRA_ISSUEKEY_PATTERN = compile(r'[A-Z]+-[0-9]+')


class IssueNotFound(Exception):
    pass


def jira_api(method='GET', path='issue', data=None):
    url = "{base}/{api}/{path}".format(
        base=settings.rest_url,
        api=JIRA_API_V2,
        path=path,
    )
    headers = {
        "Authorization": "Bearer {}".format(settings.rest_token),
        "Accept": "application/json",
        "Content-Type": "application/json",
    }
    response = request(method, url, headers=headers, data=dumps(data) if data else None, timeout=REQUEST_TIMEOUTS)
    try:
        return loads(response.text)
    except JSONDecodeError as e:
        logging.exception("Json parse error: {}".format(e))
        logging.debug("Response: {}".format(response.text))
        return None


def jira_get_issue(issue_key) -> str:
    data = jira_api('GET', 'issue/{}?fields=summary'.format(issue_key))
    if not data or data.get('errorMessages'):
        raise IssueNotFound(data.get('errorMessages')[0] if data else 'No response')
    else:
        # print("[DEBUG]", json.dumps(data, sort_keys=True, indent=4, separators=(",", ": ")))
        return "{} {}".format(data['key'], data['fields']['summary'])


def jira_is_issue_exits(issue_key: str) -> bool:
    try:
        issue = jira_get_issue(issue_key)
        return bool(issue)
    except IssueNotFound:
        pass
    except Exception as e:
        logging.exception(e)
        pass

    return False


def jira_extract_issue_key(summary: str) -> str or None:
    if not summary:
        return None

    return (JIRA_ISSUEKEY_PATTERN.findall(summary.split()[0] or '') or [None])[0]


def jira_timelog(issue_key, date, time_spend, comment):
    try:
        response = jira_api('POST', 'issue/{}/worklog'.format(issue_key), {
            "timeSpent": time_spend,
            "comment": comment,
            "started": "{}T00:00:00.000+0000".format(date)
        })
        logging.debug("Worklog response: {}".format(response))
    except Exception as e:
        logging.exception(e)
        return "Exception: {}".format(e)


def jira_is_issues_exists(issue_keys_list):
    jql = " or ".join([f"issuekey={i}" for i in issue_keys_list])
    query = {
        "jql": jql,
        "fields": ['summary'],
        "maxResults": len(issue_keys_list) + 1,
    }
    response = jira_api('POST', 'search?jql', data=query)
    if response.get('errorMessages'):
        return response.get('errorMessages')[0]


def jira_check_connection():
    response = jira_api('GET', 'serverInfo')
    logging.debug("jira_check_connection response: {}".format(response))
    return None if response.get('version') else 'Failed to get Jira version'
