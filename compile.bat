@echo off
set DIR_SCRIPT=%~dp0
set DIR_ROOT=%DIR_SCRIPT:~0,-1%
set DIR_BUILD=%DIR_ROOT%\distr
@rem # Lib dir separator is ";"
set DIR_LIB=%DIR_ROOT%\venv\Lib\site-packages
if "%1" == "" (set BUILD_VERSION=v0.0-dev) else (set BUILD_VERSION=%1)
if "%2" == "" (set FILE_ARCHIVE=%DIR_BUILD%\pyTimeSched-%BUILD_VERSION%.zip) else (set FILE_ARCHIVE=%2)
set FILE_APP_ICON=%DIR_ROOT%\gui\icons\icon.ico

cd /d %DIR_ROOT%

call ci\build_scripts\load_venv.bat
call ci\build_scripts\exit_on_error.bat

call ci\build_scripts\bump_version.bat
call ci\build_scripts\exit_on_error.bat

call ci\build_scripts\compile_onefile.bat
call ci\build_scripts\exit_on_error.bat

call ci\build_scripts\archive_distr.bat
call ci\build_scripts\exit_on_error.bat

call ci/build_scripts/banner.bat BUILD SUCCESSFULLY: %FILE_ARCHIVE%
exit 0
