from logging import info, getLogger, ERROR
from sys import exit, argv, version
from asyncqt import QEventLoop
from asyncio import set_event_loop, get_event_loop, start_server, open_connection
from signal import signal, SIGINT, SIG_DFL
from click import command, option
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import Qt
from gui.gui import PyTimeSchedWindow


try:
    from pytimesched_release_version import pytime_version
except ModuleNotFoundError:
    pytime_version = '[dev]'
except ImportError:
    pytime_version = '[unknown]'

PYTIMESCHED_HOST = '127.7.7.7'
PYTIMESCHED_PORT = 35513

FALLBACK_ERRORS = [  # Port already in use
    98,              # Linux
    10048,           # Windows
]

VERSION_TEXT = '''
pyTimeSched {} © ilmenshik@gmail.com
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
'''


@command()
@option('--debug', is_flag=True)
@option('-b', '--bump-version', 'app_version', default=pytime_version, metavar='VERSION', help='pyTimeSched Build version')
@option('-p', '--port', default=PYTIMESCHED_PORT, metavar='PORT', help='Port')
@option('-V', '--version', 'show_version', is_flag=True)
def main(debug, app_version, port, show_version):
    if show_version:
        print(VERSION_TEXT.strip().format(app_version))
        exit(0)

    if hasattr(Qt, 'AA_EnableHighDpiScaling'):
        info('Using high DPI scaling')
        QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)

    if hasattr(Qt, 'AA_UseHighDpiPixmaps'):
        info('Using high DPI pixmaps')
        QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)

    app = QApplication(argv)
    app.setApplicationVersion(app_version)

    scale_percents = round(app.desktop().logicalDpiX() / 96 * 100)
    info(f'Screen scale: {scale_percents}%')

    loop = QEventLoop(app)
    set_event_loop(loop)

    window = PyTimeSchedWindow(app, debug)

    python_release = int(version.split('.')[0])
    python_version = int(version.split('.')[1])
    if python_release >= 3 and python_version >= 10:
        coro = start_server(window.handle_echo, PYTIMESCHED_HOST, port)
    else:
        loop = get_event_loop()
        coro = start_server(window.handle_echo, PYTIMESCHED_HOST, port, loop=loop)

    try:
        server = loop.run_until_complete(coro)
    except OSError as e:
        if e.errno in FALLBACK_ERRORS:
            print("Port is busy, it looks like the application is running. Trying to focus on it.")
            loop.run_until_complete(tcp_echo_client('focus', PYTIMESCHED_HOST, port))
            loop.close()
            exit()
        else:
            raise e

    window.start(debug)
    info('Serving on {}'.format(server.sockets[0].getsockname()))
    loop.run_until_complete(server.wait_closed())
    server.close()
    loop.close()


async def tcp_echo_client(message, host, port):
    reader, writer = await open_connection(host, port)
    info('Send: {}'.format(message))
    writer.write(message.encode())
    # data = await reader.read(100)
    # nfo('Received: %r' % data.decode())
    writer.close()


if __name__ == '__main__':
    getLogger('asyncqt').setLevel(ERROR)
    signal(SIGINT, SIG_DFL)  # ignore KeyboardInterrupt
    main()
