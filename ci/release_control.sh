#!/usr/bin/env sh
set -e
echo "CI_COMMIT_TAG=$CI_COMMIT_TAG"
echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"
echo "Linux: $LINUX_RELEASE_JOB_ID/$LINUX_RELEASE_FILE"
echo "Windows: $WINDOWS_RELEASE_JOB_ID/$WINDOWS_RELEASE_FILE"
echo "Version: ${RELEASE_VERSION} (tag: $CI_COMMIT_TAG)"
echo "Change list:"
echo "${RELEASE_CHANGELIST}" | sed 's/^/\t/g'

export LINUX_URL="$CI_PROJECT_URL/-/jobs/$LINUX_RELEASE_JOB_ID/artifacts/raw/$LINUX_RELEASE_FILE"
export WINDOWS_URL="$CI_PROJECT_URL/-/jobs/$WINDOWS_RELEASE_JOB_ID/artifacts/raw/$WINDOWS_RELEASE_FILE"

echo "Check artifacts..."
ls -l "$LINUX_RELEASE_FILE"
ls -l "$WINDOWS_RELEASE_FILE"

if echo "$RELEASE_VERSION" | grep -Eq '^v(([0-9]+\.)+[1-9][0-9]*|[1-9][0-9]*(\.[0-9]+)+)$'; then
  echo "Release: $RELEASE_VERSION"
else
  echo "Install utilities"
  apk update
  apk add curl jq

  echo "Check release existance..."
  if curl -s "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$RELEASE_VERSION" | jq -r '.name' | grep -Eq "$RELEASE_VERSION"; then
    echo "Release $RELEASE_VERSION already exist. Removing first..."
    curl --request DELETE --header "PRIVATE-TOKEN:$CI_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$RELEASE_VERSION" > /dev/null
  fi

  echo "Release (dev): $RELEASE_VERSION"
fi

description='no description'
if [[ "$RELEASE_CHANGELIST" != '' ]]; then
    description="### Changelog:"$'\n'"$RELEASE_CHANGELIST"

fi

release-cli create --name ${RELEASE_VERSION} \
--tag-name ${RELEASE_VERSION} --ref $CI_COMMIT_SHA \
--assets-link "{\"name\": \"pyTimeSched ${RELEASE_VERSION} (Linux x64)\", \"url\": \"$LINUX_URL\", \"link_type\": \"other\"}" \
--assets-link "{\"name\": \"pyTimeSched ${RELEASE_VERSION} (Windows x64)\", \"url\": \"$WINDOWS_URL\", \"link_type\": \"other\"}" \
--description "$description"
