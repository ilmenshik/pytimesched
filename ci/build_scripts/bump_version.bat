call ci\build_scripts\banner.bat Prepare files and dirs

rmdir /s /q %DIR_BUILD%
call ci\build_scripts\exit_on_error.bat
mkdir %DIR_BUILD%
call ci\build_scripts\exit_on_error.bat
echo pytime_version='%BUILD_VERSION%' > %DIR_ROOT%\pytimesched_release_version.py
call ci\build_scripts\exit_on_error.bat
type %DIR_ROOT%\pytimesched_release_version.py
echo Release version: %BUILD_VERSION%
exit /b 0