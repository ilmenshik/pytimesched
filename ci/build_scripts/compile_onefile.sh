set -e
bash ci/build_scripts/banner.sh "Compile project into binary"
pyinstaller --name pyTimeSched --onefile \
    --paths "$DIR_LIB" \
    --distpath "${DIR_BUILD}" \
    --specpath "${DIR_BUILD}/build" \
    --workpath "${DIR_BUILD}/build" \
    main.py

chmod +x "${DIR_BUILD}/pyTimeSched"
rm -rf "${DIR_BUILD}/build"