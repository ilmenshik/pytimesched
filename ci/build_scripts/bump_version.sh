set -e
bash ci/build_scripts/banner.sh "Prepare files and dirs"
rm -rf "${DIR_BUILD}"
mkdir "${DIR_BUILD}"
echo "pytime_version='${BUILD_VERSION}'" >"${DIR_ROOT}/pytimesched_release_version.py"
trap '{ rm -fv "${DIR_ROOT}/pytimesched_release_version.py" > /tmp/1; }' EXIT
echo "Release version: ${BUILD_VERSION}"
