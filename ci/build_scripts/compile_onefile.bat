call ci\build_scripts\banner.bat Compile project into binary

echo Make version file
python %DIR_ROOT%\utils\version_file.py -i "metadata.yml" -o "%DIR_ROOT:\=/%/version_file.txt" -v "%BUILD_VERSION%"
call ci\build_scripts\exit_on_error.bat

echo Build...
python -m PyInstaller --name pyTimeSched --onefile ^
--paths "%DIR_LIB%" ^
--distpath "%DIR_BUILD%" ^
--version-file "%DIR_ROOT%\version_file.txt" ^
--specpath "%DIR_BUILD%\build" ^
--workpath "%DIR_BUILD%\build" ^
--icon %FILE_APP_ICON% ^
--windowed ^
main.py
set er=%errorlevel%

del /f /q %DIR_ROOT%\pytimesched_release_version.py
rmdir /s /q %DIR_BUILD%\build\
exit /b %er%