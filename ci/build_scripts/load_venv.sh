set -e
bash ci/build_scripts/banner.sh "Load virtual environment $1"

echo "Check python"
whereis python

if [[ ! -d venv ]]; then
    echo "Creating venv"
    python3 -m venv --upgrade-deps ${1:-venv}
fi

echo "Activate"
source "venv/bin/activate"
python --version
whereis pip

echo "Install reqs"
pip install -r requirements.txt
echo "Done"
