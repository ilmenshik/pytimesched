call ci\build_scripts\banner.bat Load virtual environment

echo Check python
where python

if not exist venv\ (
    echo Creating venv
    python -m venv --upgrade-deps venv
)

echo Activate
call venv\Scripts\activate.bat
python --version
where pip

echo Install reqs
pip install -r requirements.txt
call ci\build_scripts\exit_on_error.bat
echo "Done"
exit /b 0
