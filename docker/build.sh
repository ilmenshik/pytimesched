DIR_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)"

# Defaults
PYTHON_VERSION='3.8'
DOCKER_TAG='gui'

# Arg parse
while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--python)
            if ! [[ $2 =~ ^[0-9]+(\.[0-9]+)+$ ]]; then
                echo "[ERROR] Wrong python version: $2"
                exit 1
            fi
            PYTHON_VERSION="$(echo "$2" | sed -r 's/^([0-9]+\.[0-9]+).*/\1/g')"
            shift # past argument
        ;;
        -t|--tag)
            DOCKER_TAG="$2"
            shift # past argument
        ;;
        -*|--*)
            echo "[ERROR] Unknown option: $1"
            exit 1
        ;;
    esac

    shift # past value
done

if [[ "$PYTHON_VERSION" =~ ^3\.(8|9)$ ]]; then
    UBUNTU_VERSION='20.04'
    REQ_VERSION='3.8'
elif [[ "$PYTHON_VERSION" =~ ^3\.(10|11)$ ]]; then
    UBUNTU_VERSION='22.04'
    REQ_VERSION='3.10'
else
    echo "[ERROR] Unsupported version: $PYTHON_VERSION"
    exit 1
fi

# Build
set -e
cd "$DIR_ROOT"
docker build -t "$DOCKER_TAG" \
    --build-arg PYTHON_BASE_VERSION=$PYTHON_VERSION \
    --build-arg UBUNTU_VERSION=$UBUNTU_VERSION \
    --build-arg PYTHON_REQ_VERSION=$REQ_VERSION \
    -f "docker/Dockerfile" .

# Debug info
echo
docker image ls "$DOCKER_TAG"
