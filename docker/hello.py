from sys import argv
from PyQt5.QtWidgets import QApplication, QWidget, QLabel


if __name__ == "__main__":
    app = QApplication(argv)
    w = QWidget()
    w.setGeometry(300, 300, 250, 150)
    w.setWindowTitle("My First Qt App")
    label = QLabel("Hello World", w)
    label.setToolTip("This is a <b>QLabel</b> widget with Tooltip")
    label.resize(label.sizeHint())
    label.move(80, 50)
    w.show()
    app.exec_()
