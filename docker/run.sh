DIR_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)"

# Defaults
CUSTOM_PORT='54321'
UBUNTU_VERSION='20.04'
DOCKER_TAG='gui'
USE_SHELL=false

# Arg parse
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--port)
            if [[ $2 =~ ^[0-9]+$ && $2 -ge 1000 && $2 -le 65535 ]]; then
                CUSTOM_PORT="$2"
            fi
            shift # pass argument
        ;;
        -t|--tag)
            DOCKER_TAG="$2"
            shift # pass argument
        ;;
        --version)
            # ignore
            shift # pass argument
        ;;
        -s|--shell)
            USE_SHELL=true
        ;;
        -*|--*)
            echo "[ERROR] Unknown option: $1"
            exit 1
        ;;
        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
        ;;
    esac

    shift # pass value
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [[ $USE_SHELL == true ]]; then
    docker run --rm -it \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v "$DIR_ROOT":/app \
        -w /app \
        -e DISPLAY="$DISPLAY" \
        -u qtuser \
        $DOCKER_TAG bash
else
    docker run --rm -it \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v "$DIR_ROOT":/app \
        -w /app \
        -e DISPLAY="$DISPLAY" \
        -u qtuser \
        $DOCKER_TAG bash -c "python --version && python main.py --use-temp-dir --debug --version 'docker-$DOCKER_TAG'"
fi
