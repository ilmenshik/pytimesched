@echo off
SET DIR=%~dp0
cd /d %DIR%
echo WORKDIR: %DIR%

if not exist venv\ (
	echo Creating new virtual environment
	python -m venv venv
	venv\Scripts\pip install -r requirements.txt
)

start venv\Scripts\pythonw pythonw %DIR%\main.py %*
