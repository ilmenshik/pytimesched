# Changelog

## Next release
* Settings/Features:
    * New settings window instead of menu items
    * Added new feature: sort lock feature (protection for annoying misclicks)
    * New feature in progress: Activity checker (locked for now)
    * Reset after midnight by default
    * Changed CSV format and file extension
* API export:
    * Request timeouts
    * Some errors are more friendly now
    * Config validation button instead of validation on save
* Themes improvements:
    * Default theme is a part of the code now and cannot be overridden or broken
    * All themes are based on default theme, so not all parameters are required anymore
    * Fixed paint artifact with sort, search and other changes
    * Other minor improvements
* CI/CD improvements
    * More stable
    * Returned python3.8 back for Windows 7 support
    * First unit test
    * Simple launch test
    * More flexible build scripts
* Minor bug fixes and interface improvements

## v0.6
* Added Python 3.10 support
  * Use [requirements.txt](requirements.txt)
* Added high resolution monitors support (scale support)
* Added Jira API v2 export support
  * Can be chosen in REST connection config window
* Added changelog
* Bugfixes:
  * Replace new line symbols in title with spaces
* Docker support init