class Column:
    CHECKED = 0
    TITLE = 1
    COLOR = 2
    DATE = 3
    OVERALL = 4
    TODAY = 5
    TOGGLE = 6

    @staticmethod
    def count():
        return [getattr(Column, x) for x in dir(Column) if not x.startswith('_') and not callable(getattr(Column, x))]
