import logging
from shutil import move
from json import load, dump
from json.decoder import JSONDecodeError
from time import time
from datetime import datetime
from utils.text import format_time, zero_or_more
from typing import Dict


class Project:
    FIELD_OVERALL = 0x0
    FIELD_TODAY = 0x1

    idx: int = 0
    title: str = ''
    date: int = 0
    color: str = '#FFFFFF'
    time_overall: int = 0
    time_today: int = 0
    started_date: int = 0
    active: bool = False
    done: bool = False

    def __init__(self, idx: int, title: str):
        self.idx = idx
        self.title = title
        self.date = int(time())

    def get_overall(self):
        delta = self.get_started_delta()
        return self.time_overall + delta if self.active else self.time_overall

    def get_today(self):
        delta = self.get_started_delta()
        return self.time_today + delta if self.active else self.time_today

    def get_date(self):
        return datetime.fromtimestamp(self.date).strftime('%Y-%m-%d')

    def set_date(self, val: int):
        if val is None or val < 0:
            val = 0
        self.date = val

    def set_overall(self, val: int):
        if self.active:
            return
        self.time_overall = zero_or_more(val)

    def set_today(self, val: int):
        if self.active:
            return
        delta = val - self.time_today
        self.time_overall = zero_or_more(self.time_overall + delta)
        self.time_today = zero_or_more(val)

    def get_started_delta(self):
        if not self.active:
            return 0
        delta = int(time()) - self.started_date
        return delta if delta > 0 else 0

    def start(self):
        self.started_date = int(time())
        self.active = True

    def stop(self, seconds=None):
        if self.active:
            delta = self.get_started_delta() if seconds is None else seconds
            self.time_overall += delta
            self.time_today += delta
            self.active = False

    def reset(self):
        self.time_today = 0
        if self.active:
            delta = self.get_started_delta()
            self.time_overall += delta
            self.started_date = int(time())

    def is_today(self) -> bool:
        return datetime.fromtimestamp(int(time())).strftime('%Y-%m-%d') == self.get_date()

    def toggle(self):
        self.done = not self.done

    def __str__(self):
        return "'{title}' (date: {date}, overall: {overall}, today: {today})".format(
            title=self.title,
            date=self.get_date(),
            overall=format_time(self.get_overall()),
            today=format_time(self.get_today())
        )

    def serialize(self):
        return {
           'title': self.title,
           'date': self.date,
           'color': self.color,
           'time_overall': self.time_overall,
           'time_today': self.time_today,
           'active': self.active,
           'started_date': self.started_date,
           'done': self.done
        }


class ProjectsHolder:
    __id: int = -1
    _filename: str = None
    projects: Dict[int, Project] = dict()

    def add_project(self,
                    title: str,
                    date: int = None,
                    color: str = None,
                    time_overall: int = 0,
                    time_today: int = 0,
                    active: bool = False,
                    done: bool = False,
                    started_date: int = 0,
                    ) -> Project:
        self.__id += 1
        self.projects[self.__id] = Project(self.__id, str(title))
        project = self.projects[self.__id]
        project.date = zero_or_more(date) if date is not None else int(time())
        project.color = color
        project.time_overall = zero_or_more(time_overall)
        project.time_today = zero_or_more(time_today)
        project.active = bool(active)
        project.done = bool(done)
        project.started_date = zero_or_more(started_date)
        return self.projects[self.__id]

    def stop_all(self, seconds=None):
        for _idx in self.projects.keys():
            self.projects[_idx].stop(seconds)

    def start_project(self, idx: int):
        self.stop_all()
        self.projects[idx].started_date = int(time())
        self.projects[idx].active = True

    def get_project(self, idx: int) -> Project:
        return self.projects.get(idx)

    def remove_project(self, idx):
        # TODO: (Minor) Removing a project raises error: "Project not found at row 0 (update)"
        if not self.projects.get(idx):
            return
        del self.projects[idx]

    def get_export_data(self, attr: str, idxs: list = None) -> list or None:
        if not self.projects:
            return

        if not hasattr(self.projects[next(iter(self.projects))], attr):
            return

        return [{
            "project": self.projects[pid].title,
            "hours": round(getattr(self.projects[pid], attr) / 3600, 2),
            "date": self.projects[pid].get_date()
        } for pid in self.projects.keys() if (not idxs or pid in idxs) and getattr(self.projects[pid], attr)]

    @staticmethod
    def get_export_line_for_project(project: Project, attr: str) -> str:
        return "{date};{time};{title}".format(
            date=project.get_date(),
            time=format_time(getattr(project, attr)),
            title=project.title
        )

    def export(self, attr: str, idxs: list = None) -> str:
        if not self.projects:
            return ''

        if not hasattr(self.projects[next(iter(self.projects))], attr):
            return ''

        return "\n".join(
            self.get_export_line_for_project(self.projects[pid], attr) for pid in self.projects.keys() if
            (not idxs or pid in idxs) and getattr(self.projects[pid], attr)
        )

    def parse(self, data):
        for project_data in data:
            self.add_project(
                title=project_data.get('title'),
                date=project_data.get('date'),
                color=project_data.get('color'),
                time_overall=project_data.get('time_overall'),
                time_today=project_data.get('time_today'),
                active=project_data.get('active'),
                done=project_data.get('done'),
                started_date=project_data.get('started_date'),
            )

    def load(self, from_file):
        try:
            self._filename = from_file
            with open(from_file, 'r') as json_file:
                data = load(json_file)
                self.parse(data)
        except FileNotFoundError:
            logging.info("Projects file '{}' not found".format(from_file))
        except JSONDecodeError:
            logging.exception("Failed to parse projects data file")

    def serialize(self) -> list:
        return [self.projects[x].serialize() for x in self.projects.keys()]

    def save(self):
        tmp_buff = self._filename + '.tmp'
        dump(self.serialize(), open(tmp_buff, 'w'), indent=4)
        move(tmp_buff, self._filename)


projects = ProjectsHolder()
