#!/bin/bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"

source ci/build_scripts/load_venv.sh
python main.py $@
