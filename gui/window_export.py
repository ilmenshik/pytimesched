from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog

from gui.windows.text_editor import Ui_DialogTextEditor


class ExportWindow(QDialog, Ui_DialogTextEditor):
    def __init__(self, parent, text=''):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.setWindowIcon(parent.icons.app)
        self.setWindowTitle("Export")
        self.ctr_text_editor.setPlainText(text)
        self.ctr_text_editor.setFocus()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape or event.key() == (Qt.Key_Control and (Qt.Key_Return or Qt.Key_Enter)):
            self.close()
