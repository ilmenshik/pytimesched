from time import sleep
import webbrowser
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, QSize, QThread, pyqtSignal
from gui.icons import PyTimeSchedIcons
from utils.updater import get_newer_tag, get_release_url
from utils.settings import settings, UPDATE_INTERVAL_SEC


class UpdaterThread(QThread):
    dataReady = pyqtSignal(str)
    _busy = False

    def __init__(self, current_version):
        super().__init__()
        self.data = None
        self.current_version = current_version

    def run(self):
        while True:
            if self._busy:
                continue

            self._busy = True
            try:
                settings.logger.debug("Checking updates... ({})".format(self.current_version))
                new_version = get_newer_tag(self.current_version)
                if new_version:
                    self.dataReady.emit(new_version)
                    return
            finally:
                self._busy = False

            sleep(UPDATE_INTERVAL_SEC)


class UpdaterLabelWidget(QLabel):
    icons: PyTimeSchedIcons
    url: str

    def __init__(self, icons: PyTimeSchedIcons = None, parent=None):
        super(UpdaterLabelWidget, self).__init__(parent)
        self.icons = icons
        self.setMinimumSize(1, 1)  # needed to be able to scale down the image
        self.setMaximumSize(QSize(30, 20))
        self.setAlignment(Qt.AlignCenter)
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.leaveEvent(None)
        self.setMaximumWidth(self.pixmap().width())
        self.mousePressEvent = lambda e: self.open_update_url()
        self.setVisible(False)

    def set_version(self, new_version: str):
        self.setToolTip('New version {} available\nClick to visit Gitlab'.format(
            new_version
        ))
        self.url = get_release_url(new_version)
        self.setVisible(True)

    def open_update_url(self):
        if not self.url:
            return

        try:
            webbrowser.open(self.url)
        except Exception as e:
            settings.logger.exception(e)

    def enterEvent(self, event):
        self.setPixmap(self.icons.new_hover.scaledToHeight(20, Qt.FastTransformation))

    def leaveEvent(self, event):
        self.setPixmap(self.icons.new.scaledToHeight(20, Qt.FastTransformation))
