from typing import List
from PyQt5.QtCore import pyqtSignal, Qt, QRect
from PyQt5.QtWidgets import QStyledItemDelegate, QStyle
from PyQt5.QtGui import QPen, QColor, QPalette
from models.column import Column


class DelegateStyle:
    active_text: QColor = None
    active_highlighted_text: QColor = None
    active_frame: QColor = None

    date_text: QColor = None
    date_highlighted_text: QColor = None
    date_past_text: QColor = None
    date_past_highlighted_text: QColor = None

    search_suitable_text: QColor = None
    search_unsuitable_text: QColor = None


class DelegateData:
    active_project: int = None
    searched_rows: List[int] = None
    today_rows: List[int] = None


class DelegateProps:
    data: DelegateData = None
    style: DelegateStyle = None

    def __init__(self):
        self.data = DelegateData()
        self.style = DelegateStyle()


class TableItemDelegate(QStyledItemDelegate):
    cellEditingStarted = pyqtSignal(int, int)
    cellEditingEnded = pyqtSignal(int, int)
    props = DelegateProps()

    def __init__(self, parent=None):
        super(QStyledItemDelegate, self).__init__(parent)

    def paint(self, painter, option, index):
        option = option.__class__(option)
        if option.state & QStyle.State_HasFocus:
            option.state = option.state ^ QStyle.State_HasFocus

        self.initStyleOption(option, index)
        style = option.widget.style()
        self.props = option.widget.property('data') or DelegateProps()

        self.paint_styles(index, option)
        style.drawControl(QStyle.CE_ItemViewItem, option, painter, option.widget)
        self.paint_over(index, option, painter)

    def paint_styles(self, index, option):
        option.font.setBold(bool(self.is_active(index)))
        if index.column() == Column.TITLE:
            if self.is_active(index):
                self.try_color(option, QPalette.Text, self.props.style.active_text)
                self.try_color(option, QPalette.HighlightedText, self.props.style.active_highlighted_text)

            if self.props.data.searched_rows is not None:
                if index.row() in self.props.data.searched_rows:
                    self.try_color(option, QPalette.Text, self.props.style.search_suitable_text)
                    self.try_color(option, QPalette.HighlightedText, self.props.style.search_suitable_text)
                else:
                    color = self.props.style.search_unsuitable_text or option.palette.windowText().color()
                    color.setAlpha(80)
                    self.try_color(option, QPalette.Text, color)
                    self.try_color(option, QPalette.HighlightedText, color)

        elif index.column() == Column.DATE:
            if self.is_active(index) and self.is_today(index):
                self.try_color(option, QPalette.Text, self.props.style.active_text)
                self.try_color(option, QPalette.HighlightedText, self.props.style.active_highlighted_text)
            elif not self.is_today(index):
                self.try_color(option, QPalette.HighlightedText, self.props.style.date_past_highlighted_text)
                self.try_color(option, QPalette.Text, self.props.style.date_past_text)

        elif index.column() in [Column.OVERALL, Column.TODAY]:
            if self.is_active(index):
                self.try_color(option, QPalette.Text, self.props.style.active_text)
                self.try_color(option, QPalette.HighlightedText, self.props.style.active_highlighted_text)

    def paint_over(self, index, option, painter, width=2):
        if index.column() != Column.TITLE or not self.props.style.active_frame or not self.is_active(index):
            return

        frame = QRect(option.rect)
        painter.setPen(QPen(self.props.style.active_frame, width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        painter.drawLine(frame.topLeft(), frame.topRight())
        painter.drawLine(frame.bottomLeft(), frame.bottomRight())
        painter.drawLine(frame.topLeft(), frame.bottomLeft())
        painter.drawLine(frame.topRight(), frame.bottomRight())

    def is_active(self, index) -> bool:
        return self.props.data.active_project is not None and index.row() == self.props.data.active_project

    def is_today(self, index) -> bool:
        return index.row() in (self.props.data.today_rows or [])

    def createEditor(self, parent, option, index):
        result = super(TableItemDelegate, self).createEditor(parent, option, index)
        if result:
            self.cellEditingStarted.emit(index.row(), index.column())
        return result

    def destroyEditor(self, editor, index):
        super(TableItemDelegate, self).destroyEditor(editor, index)
        return self.cellEditingEnded.emit(index.row(), index.column())

    @staticmethod
    def try_color(option, cls, color):
        if color:
            option.palette.setColor(cls, color)
