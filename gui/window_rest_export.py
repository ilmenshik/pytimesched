from PyQt5.QtCore import Qt, QDate
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtWidgets import QDialog, QCalendarWidget

from gui.windows.rest_export import Ui_dialog_rest_export


class RestExportWindow(QDialog, Ui_dialog_rest_export):
    accepted: bool = False
    use_table: False
    total_h: float = 0
    count: int = 0
    init_date: str = None
    stored_color_selected_background: QColor = None
    stored_color_selected_text: QColor = None

    def __init__(self, parent=None, use_table=False, init_date=None, total_h=0, count=0):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.use_table = use_table
        self.total_h = total_h
        self.count = count
        self.init_date = init_date
        self.stored_color_selected_background = self.ctr_calendar.palette().color(QPalette.Highlight)
        self.stored_color_selected_text = self.ctr_calendar.palette().color(QPalette.HighlightedText)

        self.setWindowIcon(parent.icons.app)
        self.setWindowTitle("Export via REST")
        self.btn_export.clicked.connect(self.export)
        self.btn_export.setFocus()
        self.btn_cancel.clicked.connect(self.close)
        self.ctrl_use_table.clicked.connect(lambda: self.toggle_calendar(not self.ctrl_use_table.isChecked()))
        self.ctrl_use_table.setChecked(use_table)
        self.ctr_calendar.clicked[QDate].connect(self.update_status_text)
        self.toggle_calendar(not use_table)
        self.update_status_text()
        if init_date:
            splits = init_date.split('-')
            self.ctr_calendar.setSelectedDate(QDate(int(splits[0]), int(splits[1]), int(splits[2])))

    def update_status_text(self):
        if self.use_table:
            txt = "Export {n} projects ({h}h)".format(n=self.count, h=self.total_h)
        else:
            txt = "Export {n} projects ({h}h) to {d}".format(n=self.count, d=self.get_date(), h=self.total_h)

        self.lab_export_status.setText(txt)

    def get_date(self):
        return self.ctr_calendar.selectedDate().toString('yyyy-MM-dd')

    def export(self):
        self.accepted = True
        self.use_table = self.ctrl_use_table.isChecked()
        self.close()

    def toggle_calendar(self, enabled: bool):
        self.ctr_calendar.setEnabled(enabled)
        self.use_table = not enabled
        self.update_status_text()
        self.set_calendar_pal(enabled)

    def set_calendar_pal(self, enabled: bool):
        pal = self.ctr_calendar.palette()
        pal.setColor(
            QPalette.Highlight,
            self.stored_color_selected_background if enabled else QColor(0, 0, 0, 0)
        )
        pal.setColor(
            QPalette.HighlightedText,
            self.stored_color_selected_text if enabled else self.ctr_calendar.palette().color(QPalette.Text)
        )
        self.ctr_calendar.setPalette(pal)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

        if event.key() == (Qt.Key_Control and (Qt.Key_Return or Qt.Key_Enter)):
            self.export()
            self.close()
