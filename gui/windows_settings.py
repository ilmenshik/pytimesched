from os.path import isfile, join
from os import listdir

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QDialogButtonBox

from utils.settings import settings, DEFAULT_SETTINGS, REST_TYPES, DIR_THEMES, THEME_FILE_EXT
from models.column import Column
from gui.windows.settings import Ui_dialog_settings


class SettingsWindow(QDialog, Ui_dialog_settings):
    accepted: bool = False

    def __init__(self, parent=None, callback_rest=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.setWindowIcon(parent.icons.app)
        self.load_settings()
        self.sel_api.currentTextChanged.connect(self.toggle_api_form)
        self.ctr_button_box.button(QDialogButtonBox.Save).clicked.connect(self.save_settings)
        self.btn_api_check.clicked.connect(lambda: self.api_check(callback_rest))

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

        if event.key() == (Qt.Key_Control and (Qt.Key_Return or Qt.Key_Enter)):
            self.save_settings()
            self.close()

    def fix_protocol(self):
        if not self.inp_url.text().startswith('http'):
            self.inp_url.setText("http://{}".format(self.inp_url.text()))

    def api_check(self, callback):
        self.fix_protocol()
        callback()

    def save_settings(self):
        self.fix_protocol()
        self.accepted = True
        settings.activity_checks = self.chk_activity_checks.isChecked()
        settings.auto_check_update = self.chk_updater.isChecked()
        settings.lock_sort = self.sel_lock_sort.currentData()
        settings.popup_to_mouse = self.chk_popup_to_mouse.isChecked()
        settings.reset_after_midnight = self.chk_reset_after_midnight.isChecked()
        settings.rest_login = self.inp_login.text()
        settings.rest_token = self.inp_token.text()
        settings.rest_type = self.sel_api.currentData()
        settings.rest_url = self.inp_url.text()
        settings.rest_use = bool(self.sel_api.currentData())
        settings.use_theme = self.sel_theme.currentData()
        settings.workhours = self.inp_workhours.value()
        settings.save()

    def load_settings(self):
        # Combo
        self.load_themes(settings.use_theme)
        self.load_apis(settings.rest_type)
        self.load_locks(settings.lock_sort)

        # Inputs
        self.inp_url.setText(settings.rest_url or '')
        self.inp_login.setText(settings.rest_login or '')
        self.inp_token.setText(settings.rest_token or '')
        self.inp_workhours.setValue(settings.workhours or 0)

        # Checkboxes
        self.chk_popup_to_mouse.setChecked(settings.popup_to_mouse)
        self.chk_updater.setChecked(settings.auto_check_update)
        self.chk_reset_after_midnight.setChecked(settings.reset_after_midnight)
        self.chk_activity_checks.setChecked(settings.activity_checks)

        self.set_api_form_enabled(settings.rest_use)

    def load_locks(self, current_col):
        locks = [
            [None, "<unlocked>"],
            [Column.TITLE, 'Title'],
            [-Column.TITLE, 'Title (descending)'],
            [Column.DATE, 'Created'],
            [-Column.DATE, 'Created (descending)'],
            [Column.OVERALL, 'Time today'],
            [-Column.OVERALL, 'Time today (descending)'],
            [Column.TODAY, 'Time overall'],
            [-Column.TODAY, 'Time overall (descending)'],
        ]
        self.sel_lock_sort.clear()
        for idx, lock in enumerate(locks):
            self.sel_lock_sort.addItem(lock[1], lock[0])
            if lock[0] == current_col:
                self.sel_lock_sort.setCurrentIndex(idx)

        if self.sel_theme.currentIndex() < 0:
            self.sel_theme.setCurrentIndex(0)

    def load_themes(self, current_theme):
        themes = [[None, '<default>']] + self.get_all_themes_files()
        self.sel_theme.clear()
        for idx, theme in enumerate(themes):
            self.sel_theme.addItem(theme[1], theme[0])
            if theme[0] == current_theme:
                self.sel_theme.setCurrentIndex(idx)

        if self.sel_theme.currentIndex() < 0:
            self.sel_theme.setCurrentIndex(0)

    def load_apis(self, use_api):
        self.sel_api.clear()
        for idx, api_code in enumerate(REST_TYPES.keys()):
            self.sel_api.addItem(REST_TYPES[api_code], api_code)
            if api_code == use_api:
                self.sel_api.setCurrentIndex(idx)

    def set_api_form_enabled(self, enabled):
        self.inp_url.setEnabled(enabled)
        self.inp_login.setEnabled(enabled)
        self.inp_token.setEnabled(enabled)
        self.btn_api_check.setEnabled(enabled)

    def toggle_api_form(self):
        self.set_api_form_enabled(bool(self.sel_api.currentData()))

    @staticmethod
    def is_valid_theme_file(filename):
        return all([
            filename.endswith(THEME_FILE_EXT),
            not filename.startswith('_'),
            filename != 'default' + THEME_FILE_EXT,
            isfile(join(DIR_THEMES, filename)),
        ])

    @staticmethod
    def get_all_themes_files():
        return [
            [f.replace(THEME_FILE_EXT, '')]*2 for f in listdir(DIR_THEMES) if SettingsWindow.is_valid_theme_file(f)
        ]
