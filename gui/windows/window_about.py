# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/src/about.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DialogAbout(object):
    def setupUi(self, DialogAbout):
        DialogAbout.setObjectName("DialogAbout")
        DialogAbout.setWindowModality(QtCore.Qt.ApplicationModal)
        DialogAbout.resize(360, 220)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DialogAbout.sizePolicy().hasHeightForWidth())
        DialogAbout.setSizePolicy(sizePolicy)
        DialogAbout.setMinimumSize(QtCore.QSize(360, 220))
        DialogAbout.setMaximumSize(QtCore.QSize(16777215, 16777215))
        DialogAbout.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogAbout)
        self.verticalLayout.setContentsMargins(30, -1, 30, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lab_logo = QtWidgets.QLabel(DialogAbout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lab_logo.sizePolicy().hasHeightForWidth())
        self.lab_logo.setSizePolicy(sizePolicy)
        self.lab_logo.setMinimumSize(QtCore.QSize(1, 60))
        self.lab_logo.setMaximumSize(QtCore.QSize(16777215, 60))
        self.lab_logo.setText("")
        self.lab_logo.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.lab_logo.setObjectName("lab_logo")
        self.verticalLayout.addWidget(self.lab_logo)
        self.lab_title = QtWidgets.QLabel(DialogAbout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lab_title.sizePolicy().hasHeightForWidth())
        self.lab_title.setSizePolicy(sizePolicy)
        self.lab_title.setMinimumSize(QtCore.QSize(0, 0))
        self.lab_title.setMaximumSize(QtCore.QSize(16777215, 30))
        self.lab_title.setObjectName("lab_title")
        self.verticalLayout.addWidget(self.lab_title)
        self.lab_text = QtWidgets.QLabel(DialogAbout)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lab_text.sizePolicy().hasHeightForWidth())
        self.lab_text.setSizePolicy(sizePolicy)
        self.lab_text.setTextFormat(QtCore.Qt.RichText)
        self.lab_text.setScaledContents(True)
        self.lab_text.setWordWrap(True)
        self.lab_text.setOpenExternalLinks(True)
        self.lab_text.setObjectName("lab_text")
        self.verticalLayout.addWidget(self.lab_text)
        self.buttons = QtWidgets.QDialogButtonBox(DialogAbout)
        self.buttons.setOrientation(QtCore.Qt.Horizontal)
        self.buttons.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.buttons.setObjectName("buttons")
        self.verticalLayout.addWidget(self.buttons)

        self.retranslateUi(DialogAbout)
        self.buttons.accepted.connect(DialogAbout.accept)
        self.buttons.rejected.connect(DialogAbout.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogAbout)

    def retranslateUi(self, DialogAbout):
        _translate = QtCore.QCoreApplication.translate
        DialogAbout.setWindowTitle(_translate("DialogAbout", "About {version}"))
        self.lab_title.setText(_translate("DialogAbout", "{version}"))
        self.lab_text.setText(_translate("DialogAbout", "<html><head/><body><p>Written by <a href=\"mailto:ilmenshik@gmail.com\"><span style=\" text-decoration: underline; color:#0000ff;\">D.Zimulkin</span></a><br/>Documentation can be found on the <a href=\"https://gitlab.com/ilmenshik/pytimesched/-/blob/master/README.md\"><span style=\" text-decoration: underline; color:#0000ff;\">project page</span></a><br/><br/>Release under GPLv3 license</p></body></html>"))
