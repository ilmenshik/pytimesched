from re import compile
from requests import post
from requests.exceptions import ConnectTimeout, ReadTimeout, SSLError
from sys import exit
from datetime import datetime
from time import mktime
from os.path import join, isdir
from pathlib import Path
from json import dumps

from PyQt5.QtCore import (
    QFile,
    Qt,
    QTextStream,
    QTimer,
)
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QMenu,
    QAction,
    QMessageBox,
    QSystemTrayIcon,
    QAbstractItemView,
    QHeaderView,
    QTableWidgetItem,
    QLabel,
)
from PyQt5.QtGui import QColor, QFontDatabase, QFont, QCursor, QPalette

from gui.window_export import ExportWindow
from gui.widget_image_button import ImageButtonWidget
from gui.widget_updater_label import UpdaterLabelWidget, UpdaterThread
from gui.window_project_color import ProjectColorDialog
from gui.delegate_table_item import TableItemDelegate, DelegateProps
from gui.window_templates import TemplatesWindow
from gui.window_rest_export import RestExportWindow
from gui.windows_about import AboutWindow
from gui.windows_settings import SettingsWindow
from gui.windows.main import Ui_MainWindow
from gui.icons import PyTimeSchedIcons

from models.projects import projects, Project
from models.column import Column

from utils.settings import (
    settings,
    FILE_GEOMETRY,
    DIR_EXPORT,
    FILE_PROJECTS,
    FILES_FONT,
    FONT_FAMILY,
    DIR_ICONS,
    DIR_THEMES,
    THEME_DEFAULT,
    THEME_FILE_EXT,
    REQUEST_TIMEOUTS,
)
from utils.text import (
    now_format,
    generate_export_filename,
    format_time,
    zero_or_more,
    parse_time,
    REGEX_VALID_DATE,
    REGEX_VALID_TIME,
)
from utils.misc import FLAGS_EDITABLE, FLAGS_SELECTABLE, DATA_ROLE
from utils.jira_export import (
    jira_is_issues_exists,
    jira_timelog,
    jira_extract_issue_key,
    jira_check_connection,
)

REGEX_VALID_THEME_LINE = compile(r'^(Disabled\.)?[A-Za-z]+[\s\t]+#[A-Fa-f0-9]{,6}$')
REGEX_VALID_THEME_CUSTOM_LINE = compile(r'^(Project)\.[A-Za-z.]+[\s\t]+#[A-Fa-f0-9]{,6}$')


class Reset:
    ALL = 0
    SELECTED = 1
    OUTDATED = 2


class PyTimeSchedWindow(QMainWindow, Ui_MainWindow):
    debug = False
    tray: QSystemTrayIcon = None
    icons: PyTimeSchedIcons = None
    app: QApplication = None
    logger: None
    timer: QTimer = QTimer()
    last_hour = None
    sort_backuped = False
    sort_order = -1
    sort_by_col_id = -1
    color_today = None
    updater_label: UpdaterLabelWidget = None
    updater_thread: UpdaterThread = None
    props: DelegateProps = None

    def __init__(self, app, debug=False):
        super(PyTimeSchedWindow, self).__init__()
        # Attributes
        self.debug = debug
        self.logger = settings.logger
        self.icons = PyTimeSchedIcons(DIR_ICONS)

        # Setup UI
        self.app = app
        self.app.setStyle("Fusion")
        self.setupUi(self)
        self.setWindowTitle("{} {}".format(self.windowTitle(), self.app.applicationVersion()))
        self.setWindowIcon(self.icons.app)

        # Setup controls
        self.load_geometry()
        self.init_fonts()
        self.init_tray(self.icons.app)
        self.init_log_widget()
        self.init_search()
        self.init_buttons()
        self.init_top_menu()
        self.update_templates()
        self.set_theme(settings.use_theme)
        self.init_table()
        self.load_projects()
        self.load_sort()
        self.init_ticks()
        self.init_updater()

        # Setup finished
        self.log('Init: OK')

    # ---------------------- Debug ----------------------
    def debug_method(self):
        self.logger.debug("Placeholder: {}".format('debug'))

    def init_ticks(self):
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.tick)
        self.timer.start()
        self.tick()

    def tick(self):
        try:
            if settings.reset_after_midnight and self.last_hour and self.last_hour > datetime.now().hour:
                self.reset(Reset.OUTDATED)

            self.last_hour = datetime.now().hour
            self.refresh_interface()
        except Exception as e:
            self.logger.exception(e)
            raise e

    # ---------------------- Main window ----------------------
    def start(self, debug):
        if debug:
            self.log('Debug is enabled')

        self.show()

    def quit(self):
        self.tray.hide()
        self.save_geometry()
        exit(0)

    def closeEvent(self, event):
        event.ignore()
        self.hide()

    def focus(self):
        self.logger.debug('Found focus')
        self.show()
        self.activateWindow()
        if settings.popup_to_mouse:
            self.move(
                QCursor().pos().x() - self.size().width() // 2,
                QCursor().pos().y() - self.size().height() // 2
            )

    def load_geometry(self):
        file = QFile(FILE_GEOMETRY)
        try:
            if file.open(QFile.ReadOnly):
                byte_arr = file.readAll()
                self.logger.debug('Restored geometry from: {}'.format(FILE_GEOMETRY))
                self.restoreGeometry(byte_arr)
        except Exception as e:
            self.logger.debug(e)
        finally:
            file.close()

    def save_geometry(self):
        byte_arr = self.saveGeometry()
        file = QFile(FILE_GEOMETRY)
        file.open(QFile.WriteOnly)
        file.write(byte_arr)
        self.logger.debug('Saved geometry to: {}'.format(FILE_GEOMETRY))
        file.close()

    def init_fonts(self):
        for font_file in FILES_FONT:
            QFontDatabase.addApplicationFont(font_file)

        for font in FONT_FAMILY:
            if font in QFontDatabase().families():
                self.ctr_table.setFont(QFont(font, 9))
                return

    def toggle_main_window(self):
        if self.isActiveWindow():
            self.hide()
        else:
            self.show()
            self.activateWindow()

    def refresh_interface(self):
        self.update_table()
        self.update_status_bar()

    # ---------------------- Table ----------------------
    def init_table(self, action_cell_width=24):
        # Table
        self.ctr_table.setItemDelegate(self.get_delegate())
        self.ctr_table.selectionModel().selectionChanged.connect(self.update_status_bar)
        self.ctr_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ctr_table.setSortingEnabled(not settings.lock_sort)

        self.init_table_rows(action_cell_width)
        self.init_table_cols(action_cell_width)
        self.init_table_cells()

    def init_table_rows(self, action_cell_width):
        self.ctr_table.setAlternatingRowColors(True)  # striped table
        self.ctr_table.verticalHeader().hide()
        self.ctr_table.verticalHeader().setDefaultSectionSize(action_cell_width)  # row height

    def init_table_cells(self):
        self.ctr_table.itemClicked.connect(self.click_table_handler)
        self.ctr_table.cellActivated.connect(self.end_edit)

    def init_table_cols(self, action_cell_width):
        c = self.ctr_table.horizontalHeader()
        c.sortIndicatorChanged.connect(self.action_on_table_sort)

        c.setHighlightSections(False)  # hide column highlight
        for col in [Column.TITLE]:
            c.setSectionResizeMode(col, QHeaderView.Stretch)

        for col in [Column.TODAY, Column.OVERALL]:
            c.setSectionResizeMode(col, QHeaderView.Fixed)
            c.resizeSection(col, 100)

        for col in [Column.DATE]:
            c.setSectionResizeMode(col, QHeaderView.Fixed)
            c.resizeSection(col, 110)

        for col in [Column.CHECKED, Column.COLOR, Column.TOGGLE]:
            c.setSectionResizeMode(col, QHeaderView.Fixed)
            c.resizeSection(col, action_cell_width)

    def update_table_properties(self):
        if not self.props:
            self.props = self.ctr_table.property('data') or DelegateProps()

        self.ctr_table.setProperty('data', self.props)

    def click_table_handler(self, item: QTableWidgetItem):
        row_id = item.row()
        col_id = item.column()
        project = self.get_project(row_id)
        if not project:
            self.logger.error("Project not found at row {} (click)".format(row_id))
            return
        if col_id == Column.TOGGLE:
            self.toggle_state_project(row_id, project)
            self.set_project_row_state(row_id, project.active)

        if col_id == Column.CHECKED:
            self.toggle_done_project(project)
            self.set_project_check_icon(row_id, project.done)

        if col_id == Column.COLOR:
            ProjectColorDialog(row_id, self).open()

    def update_table_handler(self, item: QTableWidgetItem):
        self.backup_sort()
        project = self.get_project(item.row())
        if not project:
            self.logger.error("Project not found at row {} (update)".format(item.row()))
            return

        has_updates = False

        if item.column() == Column.TITLE:
            text = item.text().replace('\n', ' ')
            if text != project.title:
                has_updates = True
                item.setText(text)
                self.update_project_title(project, text)

        elif item.column() == Column.DATE:
            if item.text() != project.get_date():
                has_updates = True
                self.update_project_date(project, item.text())

        elif item.column() in [Column.OVERALL, Column.TODAY]:
            if self.update_project_time(project, item.column(), item.text()) is not None:
                has_updates = True

        if has_updates:
            self.update_project_row(item.row(), project)
            self.refresh_interface()
            self.restore_sort()

    def update_table(self):
        self.props.data.today_rows = []
        self.props.data.active_project = None
        self.props.data.searched_rows = [] if self.inp_search.text() else None
        for row_id in range(0, self.ctr_table.rowCount()):
            project = self.get_project(row_id)
            if not project:
                continue

            if project.get_date() == now_format().split(' ')[0]:
                self.props.data.today_rows.append(row_id)

            if project.active:
                self.props.data.active_project = row_id
                self.update_project_row_time(row_id, project)

            if self.inp_search.text() and self.inp_search.text().lower() in project.title.lower():
                self.props.data.searched_rows.append(row_id)

        self.update_table_properties()
        self.ctr_table.viewport().update()  # redraw

    # ---------------------- Log ----------------------
    def init_log_widget(self):
        self.btn_clear_log.setIcon(self.icons.clear_log)
        if not settings.show_log:
            self.hide_log()
        else:
            self.show_log()

    def log(self, content: str, silent: bool = False):
        if self.logger and not silent:
            self.logger.info(content)
        content = "{} {}".format(now_format(), content)
        self.ctr_text_log.appendPlainText(content)

    def clear_log(self):
        self.ctr_text_log.clear()

    def show_log(self):
        self.ctr_text_log.show()
        self.btn_clear_log.show()
        self.btn_log.setText("Hide log")
        self.btn_log.setIcon(self.icons.hide_log)

    def hide_log(self):
        self.ctr_text_log.hide()
        self.btn_clear_log.hide()
        self.btn_log.setText("Show log")
        self.btn_log.setIcon(self.icons.show_log)

    def toggle_log_widget(self):
        if self.ctr_text_log.isVisible():
            self.hide_log()
        else:
            self.show_log()

        settings.show_log = self.ctr_text_log.isVisible()
        settings.save()

    # ---------------------- Tray ----------------------
    def init_tray(self, icon):
        self.tray = QSystemTrayIcon()
        self.tray.setToolTip('pyTimeSched')
        self.tray.setIcon(icon)
        self.tray.show()
        self.tray.activated.connect(self.toggle_main_window)
        self.update_tray()

    def update_tray(self):
        quit_action = QAction("Quit", self)
        quit_action.triggered.connect(exit)

        debug_action = QAction("Debug", self)
        debug_action.triggered.connect(self.debug_method)

        tray_menu = QMenu()
        tray_menu.adjustSize()
        if self.debug:
            tray_menu.addAction(debug_action)

        tray_menu.addAction(quit_action)
        self.tray.setContextMenu(tray_menu)

    # ---------------------- Search ----------------------
    def init_search(self):
        self.btn_search_reset.clicked.connect(self.reset_search)
        self.inp_search.textChanged.connect(self.search)
        self.btn_search_reset.setVisible(False)

    def search(self):
        self.btn_search_reset.setVisible(bool(self.inp_search.text()))
        self.refresh_interface()

    def reset_search(self):
        self.inp_search.setText('')

    def focus_search(self):
        self.inp_search.setFocus()

    # ---------------------- Table sort ----------------------
    def action_on_table_sort(self, col_id, sort_order):
        self.refresh_interface()
        self.save_sort(col_id, sort_order)

    def lock_sort(self, lock: int or None):
        if lock is None:
            self.ctr_table.setSortingEnabled(True)
        else:
            self.ctr_table.setSortingEnabled(False)
            self.ctr_table.sortByColumn(
                abs(lock),
                Qt.AscendingOrder if lock > 0 else Qt.DescendingOrder
            )

    def load_sort(self):
        if settings.lock_sort is not None:
            self.lock_sort(settings.lock_sort)
            return

        self.logger.debug("Restore sorting")
        if settings.sort_by:
            self.sort_by_col_id, self.sort_order = settings.sort_by[0], settings.sort_by[1]
            self.restore_sort()

    def save_sort(self, col_id, sort_order):
        if settings.sort_by == [col_id, sort_order] or settings.lock_sort is not None:
            return

        self.logger.debug("Save sorting")
        settings.sort_by = [col_id, sort_order]
        settings.save()

    def backup_sort(self):
        """
        If any sort by column enabled update, GUI may have fails. Because row ID may be changed.
        Loops: first update changes order (row ID) and ID list will be outdated.
        Inserts: row can be sorted anywhere with unknown ID before DATA placed in it.
        So, backup is required before and restore after any updates.
        """
        if not self.sort_backuped:
            self.sort_by_col_id = self.ctr_table.horizontalHeader().sortIndicatorSection()
            self.sort_order = self.ctr_table.horizontalHeader().sortIndicatorOrder()
            self.ctr_table.sortItems(-1, -1)
            self.sort_backuped = True

    def restore_sort(self):
        self.ctr_table.sortItems(self.sort_by_col_id, self.sort_order)
        self.sort_backuped = False

    # ---------------------- Controls ----------------------
    def init_buttons(self):
        self.btn_log.clicked.connect(self.toggle_log_widget)
        self.btn_clear_log.clicked.connect(self.clear_log)
        self.btn_add.clicked.connect(lambda: self.add_project())
        self.btn_export_today.clicked.connect(lambda: self.export(Column.TODAY))
        self.btn_export_overall.clicked.connect(lambda: self.export(Column.OVERALL))
        self.btn_search_reset.clicked.connect(self.reset_search)
        self.btn_add.setIcon(self.icons.project_add)

    def init_top_menu(self):
        # Actions
        self.menu_add.triggered.connect(lambda: self.add_project())
        self.menu_toggle.triggered.connect(self.toggle_state_selected_project)
        self.menu_remove.triggered.connect(self.remove_selected_projects)
        self.menu_search.triggered.connect(self.focus_search)
        self.menu_quit.triggered.connect(self.quit)
        self.menu_export_today.triggered.connect(lambda: self.export(Column.TODAY))
        self.menu_export_overall.triggered.connect(lambda: self.export(Column.OVERALL))
        self.menu_reset_all.triggered.connect(lambda: self.reset(Reset.ALL))
        self.menu_reset_selected.triggered.connect(lambda: self.reset(Reset.SELECTED))
        self.menu_about.triggered.connect(self.show_about)
        self.menu_edit_templates.triggered.connect(self.show_edit_templates)
        self.menu_clear_color.triggered.connect(self.clear_project_colors)
        self.menu_settings.triggered.connect(self.show_settings)

        # Icons
        self.menu_add.setIcon(self.icons.project_add)
        self.menu_remove.setIcon(self.icons.project_del)

    # ---------------------- Projects ----------------------
    def add_project(self, title: str = 'New Project', project: Project = None):
        self.close_editor()
        self.ctr_table.selectionModel().reset()
        new_project = projects.add_project(title) if project is None else project
        self.save_projects()
        self.add_project_row(new_project)
        self.refresh_interface()
        self.log("Added {}".format(title))
        self.edit_project(new_project)

    def edit_project(self, project: Project):
        if not project:
            return

        row_id = self.get_project_row(project.idx)
        if row_id is None:
            self.logger.error('Row not found for project {}'.format(project.idx))
            return

        index = self.ctr_table.model().index(row_id, Column.TITLE)
        self.ctr_table.setCurrentIndex(index)
        self.ctr_table.edit(index)

    def close_editor(self):
        #self.ctr_table.commitData(self.ctr_table.focusWidget())  # TODO: decide is it useful or harmful
        self.ctr_table.closeEditor(self.ctr_table.focusWidget(), TableItemDelegate.NoHint)

    def add_project_row(self, project: Project):
        if not project:
            return
        tab = self.ctr_table  # short alias
        row_id = tab.rowCount()
        self.backup_sort()
        tab.insertRow(row_id)

        # Title + Project ID
        tab.setItem(row_id, Column.TITLE, QTableWidgetItem(project.title))
        tab.item(row_id, Column.TITLE).setData(DATA_ROLE, project.idx)

        # Date
        tab.setItem(row_id, Column.DATE, QTableWidgetItem(project.get_date()))
        tab.item(row_id, Column.DATE).setTextAlignment(Qt.AlignCenter)

        # Overall
        tab.setItem(row_id, Column.OVERALL, QTableWidgetItem(format_time(project.get_overall())))
        tab.item(row_id, Column.OVERALL).setTextAlignment(Qt.AlignCenter)

        # Today
        tab.setItem(row_id, Column.TODAY, QTableWidgetItem(format_time(project.get_today())))
        tab.item(row_id, Column.TODAY).setTextAlignment(Qt.AlignCenter)

        # Checkbox
        tab.setItem(row_id, Column.CHECKED, QTableWidgetItem())
        tab.item(row_id, Column.CHECKED).setFlags(FLAGS_SELECTABLE)
        tab.setCellWidget(
            row_id,
            Column.CHECKED,
            ImageButtonWidget(self.icons.unchecked)
        )

        # Color
        tab.setItem(row_id, Column.COLOR, QTableWidgetItem())
        tab.item(row_id, Column.COLOR).setFlags(FLAGS_SELECTABLE)
        tab.setCellWidget(row_id, Column.COLOR, QLabel())

        # Start/Stop
        tab.setItem(row_id, Column.TOGGLE, QTableWidgetItem())
        tab.item(row_id, Column.TOGGLE).setFlags(FLAGS_SELECTABLE)
        tab.setCellWidget(
            row_id,
            Column.TOGGLE,
            ImageButtonWidget(self.icons.start)
        )

        self.update_project_row(row_id, project)
        self.restore_sort()

    def update_project_row(self, row_id: int, project: Project):
        self.set_project_check_icon(row_id, project.done)
        self.ctr_table.item(row_id, Column.TITLE).setText(project.title)
        self.set_project_row_color(row_id, project.color)
        self.ctr_table.item(row_id, Column.DATE).setText(project.get_date())
        self.update_project_row_time(row_id, project)
        self.set_project_row_state(row_id, project.active)

    def remove_selected_projects(self):
        selected_rows = list(reversed(sorted(self.get_selected_rows())))
        if not selected_rows:
            return

        self.backup_sort()

        for row_id in selected_rows:
            project = self.get_project(row_id)
            if project:
                projects.remove_project(project.idx)
                self.log("Removed {}".format(project))

            self.ctr_table.removeRow(row_id)
            self.save_projects()

        self.restore_sort()
        self.refresh_interface()

    def get_project(self, row) -> Project or None:
        if row >= self.ctr_table.rowCount():
            return

        return projects.get_project(
            self.ctr_table.item(row, Column.TITLE).data(DATA_ROLE)
        )

    def get_project_row(self, project_idx: int):
        for row_id in range(0, self.ctr_table.rowCount()):
            _project = self.get_project(row_id)
            if _project and _project.idx == project_idx:
                return row_id

    def get_selected_rows(self) -> list:
        return list(set([x.row() for x in self.ctr_table.selectedItems()]))

    def save_projects(self):
        self.logger.debug("Saving projects to {}".format(FILE_PROJECTS))
        projects.save()
        self.logger.debug("Projects saved")

    def load_projects(self):
        self.logger.debug("Loading projects from '{}'".format(FILE_PROJECTS))
        projects.load(FILE_PROJECTS)
        for pid in projects.projects:
            self.add_project_row(projects.get_project(pid))

        if settings.reset_after_midnight:
            self.reset(Reset.OUTDATED)

        self.logger.debug("Loaded {} projects".format(len(projects.projects)))
        self.refresh_interface()

    # ---------------------- Projects.Color ----------------------
    def set_project_color(self, row_id: int, new_color: str):
        project = self.get_project(row_id)
        if not project:
            self.logger.error("Project not found at row {} (color)".format(row_id))
            return

        current_color = project.color
        project.color = new_color
        self.save_projects()
        self.set_project_row_color(row_id, new_color)
        self.logger.info('Changed color {} to {} for project "{}"'.format(
            current_color or 'None',
            new_color or 'None',
            project.title
        ))

    def clear_project_colors(self):
        selected_rows = list(reversed(sorted(self.get_selected_rows())))
        if not selected_rows:
            return

        for row_id in selected_rows:
            self.set_project_color(row_id, '')

    def set_project_row_color(self, row_id: int, color: str):
        self.ctr_table.cellWidget(row_id, Column.COLOR).setStyleSheet("""
            background-color: {};
            margin: 1px;
            border-radius: 2px;
            """.format(color if color else 'transparent')
        )

    # ---------------------- Projects.State ----------------------
    def toggle_state_project(self, row_id: int, project: Project):
        if project.active:
            projects.stop_all()
            self.log("Project paused: {}".format(project))
        else:
            projects.start_project(project.idx)
            self.log("Project started: {}".format(project))

        for row in range(0, self.ctr_table.rowCount()):
            self.set_project_row_state(row, False)

        self.save_projects()
        self.set_project_row_state(row_id, project.active)
        self.refresh_interface()

    def set_project_row_state(self, row_id: int, state: bool):
        self.ctr_table.cellWidget(row_id, Column.TOGGLE).setPixmap(self.icons.stop if state else self.icons.start)
        for col in [Column.TODAY, Column.OVERALL]:
            self.ctr_table.item(row_id, col).setFlags(FLAGS_SELECTABLE if state else FLAGS_EDITABLE)

    def toggle_state_selected_project(self):
        selected_rows = list(reversed(sorted(self.get_selected_rows())))
        if not selected_rows or len(selected_rows) != 1:
            return

        row_id = selected_rows[0]
        project = self.get_project(row_id)
        if not project:
            self.logger.error("Project not found at row {} (toggle)".format(row_id))
            return

        self.toggle_state_project(row_id, project)

    # ---------------------- Projects.Done ----------------------
    def toggle_done_project(self, project: Project):
        if not project:
            return

        project.toggle()
        self.save_projects()
        self.refresh_interface()
        self.log("{} {}".format(
            'Done' if project.done else 'Undone',
            project.title
        ))

    def set_project_check_icon(self, row_id, state: bool):
        self.ctr_table.cellWidget(row_id, Column.CHECKED).setPixmap(
            self.icons.checked if state else self.icons.unchecked
        )

    # ---------------------- Projects.Time ----------------------
    def update_project_row_time(self, row_id: int, project: Project):
        self.ctr_table.item(row_id, Column.TODAY).setText(format_time(project.get_today()))
        self.ctr_table.item(row_id, Column.OVERALL).setText(format_time(project.get_overall()))

    def update_project_time(self, project: Project, edit_column: int, new_time_str: str):
        if not project or project.active or not new_time_str or not REGEX_VALID_TIME.match(new_time_str):
            return

        cur_time = project.time_today if edit_column == Column.TODAY else project.time_overall
        parsed_time = parse_time(new_time_str)
        if new_time_str.startswith('-'):
            if parsed_time == 0 or cur_time == 0:
                return

            new_time = zero_or_more(cur_time - parsed_time)
        elif new_time_str.startswith('+'):
            if parsed_time == 0:
                return

            new_time = cur_time + parsed_time
        else:
            new_time = parsed_time
            if cur_time == new_time:
                return

        if edit_column == Column.TODAY:
            project.set_today(new_time)
        if edit_column == Column.OVERALL:
            project.set_overall(new_time)

        self.log("Manually set time {t} for '{p}' from {o} to {n}".format(
            t='overall' if edit_column == Column.OVERALL else 'today',
            p=project.title,
            o=format_time(cur_time),
            n=format_time(new_time)
        ))
        self.save_projects()
        return new_time

    # ---------------------- Projects.Title ----------------------
    def update_project_title(self, project: Project, new_title: str):
        if not project or not new_title or project.title == new_title:
            return

        _old_title = project.title
        project.title = str(new_title)
        self.log("Renamed project '{}' to '{}'".format(_old_title, project.title))
        self.save_projects()

    # ---------------------- Projects.Date ----------------------
    def update_project_date(self, project: Project, new_date_str: str):
        if not project or not new_date_str or not REGEX_VALID_DATE.match(new_date_str):
            return

        try:
            new_date = int(mktime(datetime.strptime(new_date_str, '%Y-%m-%d').timetuple()))
        except ValueError:
            return

        if project.date == new_date:
            return

        _old_date = project.get_date()
        project.set_date(new_date)
        self.save_projects()
        self.log("Manually set date '{}' from {} to {}".format(
            project.title,
            _old_date,
            project.get_date()
        ))

    # ---------------------- Themes ----------------------
    def load_custom_theme_params(self, line):
        # TODO: Need for something better than this props class...
        cls, state, section, color_hex = line.replace('.', ' ').strip().split()
        color = QColor(color_hex)
        if cls == 'Project':
            if state == 'Active':
                if section == 'Text':
                    self.props.style.active_text = color
                if section == 'HighlightedText':
                    self.props.style.active_highlighted_text = color
                if section == 'Frame':
                    self.props.style.active_frame = color
            elif state == 'Date':
                if section == 'Text':
                    self.props.style.date_text = color
                if section == 'HighlightedText':
                    self.props.style.date_highlighted_text = color
            elif state == 'DatePast':
                if section == 'Text':
                    self.props.style.date_past_text = color
                if section == 'HighlightedText':
                    self.props.style.date_past_highlighted_text = color
            elif state == 'Search':
                if section == 'SuitableText':
                    self.props.style.search_suitable_text = color
                if section == 'UnsuitableText':
                    self.props.style.search_unsuitable_text = color

    def load_theme(self, filename):
        file = None

        if not filename:
            return
        try:
            file = open(join(DIR_THEMES, filename + THEME_FILE_EXT), mode='r')
            return file.read()
        except Exception as e:
            self.logger.exception(e)
        finally:
            if file:
                file.close()

    def set_theme(self, theme=None):
        self.props = DelegateProps()
        pal = self.apply_theme(THEME_DEFAULT)

        theme_content = self.load_theme(theme)
        if theme_content:
            pal = self.apply_theme(theme_content, pal)

        icon_dir = join(DIR_ICONS, theme) if theme else DIR_ICONS
        if pal:
            if isdir(icon_dir):
                self.icons.load_icons(icon_dir)
            else:
                self.icons.load_icons(DIR_ICONS)

            self.app.setPalette(pal)
            if settings.use_theme != theme:
                settings.use_theme = theme
                settings.save()

            for row_id in range(0, self.ctr_table.rowCount()):
                project = self.get_project(row_id)
                if not project:
                    continue
                self.set_project_check_icon(row_id, project.done)
                self.set_project_row_state(row_id, project.active)

            self.btn_add.setIcon(self.icons.project_add)
            self.menu_add.setIcon(self.icons.project_add)
            self.menu_remove.setIcon(self.icons.project_del)
            self.init_log_widget()
            self.refresh_interface()

    @staticmethod
    def parse_palette_line(line):
        section_state = None
        section, color = line.strip().split()

        if '.' in section:
            section_state, section = section.split('.')

        if not hasattr(QPalette, section) or (section_state and not hasattr(QPalette, section_state)):
            return

        color_args = [getattr(QPalette, section), QColor(color)]
        if section_state:
            color_args.insert(0, getattr(QPalette, section_state))

        return color_args

    def apply_theme(self, theme_data, old_palette=None):
        try:
            new_palette = QPalette() if not old_palette else old_palette
            for line in theme_data.split('\n'):
                line = line.strip()
                if not line or line.startswith('#'):
                    continue

                if REGEX_VALID_THEME_LINE.match(line):
                    color_data = self.parse_palette_line(line)
                    if color_data:
                        new_palette.setColor(*color_data)
                elif REGEX_VALID_THEME_CUSTOM_LINE.match(line):
                    self.load_custom_theme_params(line)
                else:
                    self.logger.debug('Invalid theme line: {}'.format(line))

            return new_palette
        except Exception as e:
            self.logger.exception(e)

    @staticmethod
    def toggle_stylesheet(path):
        app = QApplication.instance()
        if app is None:
            raise RuntimeError("No Qt Application found.")

        file = QFile(path)
        file.open(QFile.ReadOnly | QFile.Text)
        stream = QTextStream(file)
        app.setStyleSheet(stream.readAll())

    # ---------------------- Global handlers ----------------------
    async def handle_echo(self, reader, writer):
        data = await reader.read(100)
        message = data.decode().strip()
        if 'focus' in message:
            self.focus()
        else:
            print("{} Received: {}".format(datetime.now(), message))

        writer.close()

    def keyPressEvent(self, event):
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_D:
            set_state = None
            for row_id in self.get_selected_rows():
                project = self.get_project(row_id)
                if not project:
                    continue
                if set_state is None:
                    set_state = not project.done
                if project.done == set_state:
                    continue
                self.toggle_done_project(project)
                self.set_project_check_icon(row_id, project.done)

        if event.modifiers() == Qt.AltModifier and event.key() == Qt.Key_D and self.debug:
            self.debug_method()

        if event.key() == Qt.Key_Escape:
            if self.inp_search.hasFocus() and self.inp_search.text():
                self.reset_search()
            else:
                self.hide()

    def get_delegate(self):
        delegate = TableItemDelegate(self)
        delegate.cellEditingEnded.connect(self.end_edit)
        return delegate

    def end_edit(self, *args):
        self.update_table_handler(self.ctr_table.item(args[0], args[1]))

    # ---------------------- Windows ----------------------
    def show_settings(self, tab_index=0):
        window = SettingsWindow(self, callback_rest=lambda: self.validate_rest_config(
            window.sel_api.currentData(),
            window.inp_url.text(),
            window.inp_login.text(),
            window.inp_token.text()
        ))
        window.ctr_tabs.setCurrentIndex(tab_index or 0)
        window.exec()
        if window.accepted:
            self.set_theme(settings.use_theme)
            self.lock_sort(settings.lock_sort)

    def show_about(self):
        AboutWindow(
            self,
            version_text='pyTimeSched {}'.format(self.app.applicationVersion()),
            pixmap=self.icons.app_logo,
        ).show()

    def show_export(self, text):
        ExportWindow(self, text).exec()

    def show_rest_status(self, title, text, color="#0F0"):
        msg = '''
            <h2 style="color: {color}">{title}</h2>
            {text}
        '''.format(title=title, text=text, color=color)
        box = QMessageBox(self)
        box.setWindowTitle('REST Result')
        box.setTextFormat(Qt.RichText)
        box.setText(msg)
        box.exec()

    def show_edit_templates(self):
        window = TemplatesWindow(self)
        window.parse_templates(settings.templates)
        window.exec()
        if window.accepted and settings.templates != list(window.get_templates()):
            self.logger.info('Update templates: {}'.format(window.get_templates()))
            settings.templates = list(window.get_templates())
            settings.save()
            self.update_templates()

    # ---------------------- Action export ----------------------
    def jira_export(self, hours_data: list) -> str or None:
        issue_keys = []

        for data in hours_data:
            issue_key = jira_extract_issue_key(data['project'])
            if not issue_key:
                return "Can't define issue from: {}".format(data['project'])

            issue_keys.append(issue_key)

        error = jira_is_issues_exists(issue_keys)
        if error:
            return error

        for data in hours_data:
            issue_key = jira_extract_issue_key(data['project'])
            self.logger.debug('To log: {} ({} = {})'.format(issue_key, data['date'], data['hours']))
            jira_timelog(issue_key, data['date'], "{}h".format(data['hours']), data['project'])

    def export_to_rest(self, col_id: int, limit: list):
        attr = 'time_overall' if col_id == Column.OVERALL else 'time_today'
        hours_data = projects.get_export_data(attr, limit)
        if not hours_data:
            self.show_rest_status('ERROR', 'Nothing to export', '#FF0000')
            return

        total = round(sum([x.get('hours') for x in hours_data]), 2)
        init_date = datetime.now().strftime('%Y-%m-%d')

        if col_id == Column.OVERALL:
            last_date = None
            for h in hours_data:
                if last_date and last_date != h.get('date'):
                    break
                last_date = h.get('date')
            else:
                init_date = last_date

        window = RestExportWindow(self, col_id == Column.OVERALL, init_date, total_h=total, count=len(hours_data))
        window.exec()

        if window.accepted:
            window.show()
            window.setEnabled(False)

            if not window.use_table:
                tmp = list()
                for h in hours_data:
                    h['date'] = window.get_date()
                    tmp.append(h)
                hours_data = tmp

            if settings.rest_type == 'jira_v2':
                error = self.jira_export(hours_data)
            else:
                error = self.rest_export(hours_data)

            if not error:
                self.show_rest_status(
                    'SUCCESS',
                    f'Exported {total}h {"in " + window.get_date() if not window.use_table else None}'
                )
                window.destroy()
            else:
                self.show_rest_status('ERROR', str(error), '#FF0000')
                window.destroy()
                self.export_to_rest(col_id, limit)

    def export_to_file(self, col_id: int, limit: list):
        attr = 'time_overall' if col_id == Column.OVERALL else 'time_today'
        txt = projects.export(attr, limit)
        if not txt:
            self.show_rest_status('ERROR', 'Nothing to export', '#FF0000')
            return

        file_suffix = attr.split('_')[1]

        try:
            Path(DIR_EXPORT).mkdir(parents=True, exist_ok=True)
            to_file = join(DIR_EXPORT, generate_export_filename(file_suffix))
            with open(to_file, 'w') as file:
                file.write(txt)
                self.log("Exported {} to {}".format(
                    attr.replace('_', ' '),
                    to_file
                ))
        except Exception as e:
            self.logger.exception(e)

        self.show_export(txt)

    def rest_export(self, hours_data: list) -> str or None:
        try:
            data = {
                'login': settings.rest_login,
                'token': settings.rest_token,
                "data": hours_data
            }
            self.logger.debug(f"Sending: {dumps(data)}")
            response = post(settings.rest_url, json=data)
            self.logger.info(f"Response: {response.text} - {response.reason}")
            text = response.text if response.text else response.reason
            if not text:
                text = 'Unknown response error'
            return text if text != 'OK' else None
        except Exception as e:
            self.logger.exception(e)
            return str(e)

    def export(self, col_id: int):
        limit = None
        if len(self.get_selected_rows()) != 0 and col_id == Column.OVERALL:
            limit = [self.get_project(row).idx for row in self.get_selected_rows()]

        if settings.rest_use:
            if not settings.rest_url:
                self.show_rest_status('ERROR', 'API not configured. Check settings and try again.', '#FF0000')
                self.show_settings(1)
            if settings.rest_url:
                self.export_to_rest(col_id, limit)
        else:
            self.export_to_file(col_id, limit)

    def validate_rest(self, api, url, login, token):
        if not api or not url:
            return 'URL should be defined'

        try:
            if api == 'jira_v2':
                return jira_check_connection()
            else:
                response = post(url, json={
                    'login': login,
                    'token': token,
                    "data": []
                }, timeout=REQUEST_TIMEOUTS)
                self.logger.info(f"Result: {response.text}" if response else "Unknown error")
                return response.text if response.text != 'OK' else None
        except ConnectTimeout:
            return 'Connection timeout exceeded'
        except ReadTimeout:
            return 'Read timeout exceeded. Check result before request repeat.'
        except SSLError:
            return 'Could not established SSL connection'
        except Exception as e:
            self.logger.exception(e)
            return str(e)

    def validate_rest_config(self, api=None, url=None, login=None, token=None):
        self.log("Checking REST settings...")
        error = self.validate_rest(api, url, login, token)
        if not error:
            self.show_rest_status('SUCCESS', 'Validation OK')
            return True
        else:
            self.show_rest_status('ERROR', str(error), '#FF0000')
            return False

    # ---------------------- Action reset ----------------------
    def reset(self, reset_type: int):
        limit = None
        self.backup_sort()
        is_need_saving = False

        if reset_type == Reset.SELECTED:
            limit = self.get_selected_rows()

        for row_id in range(0, self.ctr_table.rowCount()):
            if limit and row_id not in limit:
                continue

            project = self.get_project(row_id)
            if not project:
                self.logger.error("Project not found at row {} (reset)".format(row_id))
                continue

            if not project.get_today() or (reset_type == Reset.OUTDATED and project.is_today()):
                continue

            self.logger.info("Reset today for project: {}".format(project))
            is_need_saving = True
            project.reset()
            self.ctr_table.item(row_id, Column.TODAY).setText(format_time(0))

        self.restore_sort()
        self.refresh_interface()
        if is_need_saving:
            self.save_projects()

    # ---------------------- Templates ----------------------
    def update_templates(self):
        self.menuTemplates.clear()
        self.menuTemplates.addAction(self.menu_edit_templates)

        if settings.templates:
            self.menuTemplates.addSeparator()
        else:
            return

        i = 0
        for template in settings.templates:
            i += 1
            act = self.menuTemplates.addAction(template, self.use_template)
            if i <= 9:
                act.setShortcut("Ctrl+{}".format(i))
            if i == 10:
                act.setShortcut("Ctrl+0")

    def use_template(self):
        self.add_project(self.sender().text())

    # ---------------------- Updater ----------------------
    def init_updater(self):
        self.updater_label = UpdaterLabelWidget(self.icons, self)
        self.layout_status.insertWidget(1, self.updater_label)

        self.updater_thread = UpdaterThread(self.app.applicationVersion())
        self.updater_thread.dataReady.connect(self.show_updater)
        self.updater_thread.start()

    def show_updater(self, new_version):
        self.logger.debug("Found new version: {}".format(new_version))
        self.updater_label.set_version(new_version)

    # ---------------------- Statusbar ----------------------
    def update_status_bar(self):
        overall = 0
        today = 0
        selected_rows = self.get_selected_rows()
        count = len(selected_rows)
        total = range(0, self.ctr_table.rowCount())
        count_str = 'Selected'
        show_left = False

        if count <= 1:
            show_left = True
            count_str = 'Total'
            selected_rows = total

        for row in selected_rows:
            project = self.get_project(row)
            if not project:
                continue

            delta = project.get_started_delta() if project.active else 0
            overall += project.time_overall + delta
            today += project.time_today + delta

        left_time_today = settings.workhours * (60 ** 2) - today
        left_str = "{}: {}".format(
            'Left' if left_time_today >= 0 else 'Overworked',
            format_time(left_time_today if left_time_today >= 0 else -left_time_today)
        )

        status = [
            "{lim} projects: {count}".format(lim=count_str, count=len(selected_rows)),
            "Overall: {overall}".format(overall=format_time(overall)),
            "Today: {today}".format(today=format_time(today)),
        ]

        if show_left:
            status.append(left_str)

        if self.props.data.searched_rows is not None:
            status.append('Found: {}'.format(len(self.props.data.searched_rows)))

        self.lab_statusbar.setText("  |  ".join(status))
