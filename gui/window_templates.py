from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QDialogButtonBox

from gui.windows.text_editor import Ui_DialogTextEditor


class TemplatesWindow(QDialog, Ui_DialogTextEditor):
    templates: list = []
    accepted: bool = False

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.setWindowIcon(parent.icons.app)
        self.setWindowTitle("Edit templates")
        self.ctr_button_box.button(QDialogButtonBox.Ok).clicked.connect(lambda: self.update_templates())
        self.ctr_text_editor.setPlaceholderText('Templates...')

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

        if event.key() == (Qt.Key_Control and (Qt.Key_Return or Qt.Key_Enter)):
            self.update_templates()
            self.close()

    def parse_templates(self, templates: list):
        self.templates = templates
        self.ctr_text_editor.setPlainText('\n'.join(templates))

    def get_templates(self):
        return list(self.templates)

    def update_templates(self):
        self.accepted = True
        self.templates = [t for t in self.ctr_text_editor.toPlainText().strip().split('\n') if t]
