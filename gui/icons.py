from os.path import join
from PyQt5.QtGui import QIcon, QPixmap


class PyTimeSchedIcons:
    app: QIcon
    app_logo: QPixmap
    checked: QPixmap
    unchecked: QPixmap
    start: QPixmap
    stop: QPixmap
    start_icon: QIcon
    stop_icon: QIcon
    project_add: QIcon
    project_del: QIcon
    show_log: QIcon
    hide_log: QIcon
    clear_log: QIcon
    log: QIcon
    new: QIcon
    new_hover: QIcon

    def __init__(self, icon_dir):
        # Static icons
        # TODO: default path for images, load defaults if file not exist
        self.app = QIcon(join(icon_dir, 'icon.png'))
        self.app_logo = QPixmap(join(icon_dir, 'icon.png'))
        self.new = QPixmap(join(icon_dir, 'new.png'))
        self.new_hover = QPixmap(join(icon_dir, 'new_hover.png'))

        # Dynamic icons theme based
        self.load_icons(icon_dir)

    def load_icons(self, icon_dir):
        # Icons
        self.start_icon = QIcon(join(icon_dir, 'start.png'))
        self.stop_icon = QIcon(join(icon_dir, 'pause.png'))
        self.project_add = QIcon(join(icon_dir, 'project-add.png'))
        self.project_del = QIcon(join(icon_dir, 'project-delete.png'))
        self.show_log = QIcon(join(icon_dir, 'log-show.png'))
        self.hide_log = QIcon(join(icon_dir, 'log-hide.png'))
        self.clear_log = QIcon(join(icon_dir, 'log-clear.png'))

        # Images
        self.checked = QPixmap(join(icon_dir, 'checked.png'))
        self.unchecked = QPixmap(join(icon_dir, 'unchecked.png'))
        self.start = QPixmap(join(icon_dir, 'start.png'))
        self.stop = QPixmap(join(icon_dir, 'pause.png'))
