from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel, QTableWidgetItem


class ImageButtonWidget(QLabel):
    def __init__(self, image: QPixmap, parent=None):
        super(ImageButtonWidget, self).__init__(parent)
        self.setPixmap(image)
        self.setAlignment(Qt.AlignCenter)
        self.setAttribute(Qt.WA_TranslucentBackground)
