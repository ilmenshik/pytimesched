from PyQt5.QtWidgets import QColorDialog


class ProjectColorDialog(QColorDialog):
    parent = None
    row_id: int = None

    def __init__(self, row_id: int, parent=None):
        super(QColorDialog, self).__init__(parent)
        self.row_id = row_id
        self.parent = parent

    def accept(self):
        if self.currentColor() and self.currentColor().isValid():
            self.parent.set_project_color(self.row_id, self.currentColor().name())
        self.close()
