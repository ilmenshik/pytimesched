from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QDialog
from gui.windows.window_about import Ui_DialogAbout


class AboutWindow(QDialog, Ui_DialogAbout):
    def __init__(self, parent, version_text='', pixmap=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.setFixedSize(QSize(self.width(), self.height()));
        self.setWindowIcon(parent.icons.app)
        self.setWindowTitle("About {}".format(version_text))
        self.lab_title.setText(version_text)
        self.lab_text.setTextInteractionFlags(Qt.TextBrowserInteraction)
        if pixmap:
            self.lab_logo.setPixmap(pixmap.scaledToWidth(self.lab_logo.height(), Qt.FastTransformation))
