#
# python -m unittest tests/test_time_parsing.py
#
from unittest import TestCase, main
from utils.text import parse_time

# Multipliers (for easier read test data)
h = 60 * 60     # hours
m = 60          # minutes
dm = 60 * 60    # decimal minutes
s = 1          # seconds

TEST_PARSER_TIME_DATA = [
    # Input     Result(seconds)         What testing
    # Basics
    ('-',       0,                      'Only minus'),
    ('+',       0,                      'Only plus'),
    ('',        0,                      'Empty string'),
    (None,      0,                      'The None should be zero'),
    (Exception(), 0,                    'Non string class should be zero'),

    # Decimals (hours)
    ('0',       0,                      'Zero'),
    ('+0001',   1*h,                    'Ignore lead zeroes'),
    ('1',       1*h,                    'Only hours'),
    ('+5',      5*h,                    'Plus hours'),
    ('-100',    100*h,                  'Minus and more than 2 digits hours'),

    # Decimals (minutes)
    ('1.25',    int(1*h + 0.25*dm),     'Short with hours'),
    ('.5',      int(0.5*dm),            'Short decimal'),
    ('-0.9',    int(0.9*dm),            'Minus and full decimal'),
    ('-.25',    int(0.25*dm),           'Minus 2 digits short'),
    ('+.05',    int(0.05*dm),           'Plus and 2 digits'),
    ('.8',      int(.8*dm),             'Decimal more than 59 of clock format'),
    ('.99',     int(.99*dm),            'Almost 1h in decimal format'),
    ('-0.166',  int(.17*dm),            'In decimal format round up to 2 digits'),

    # Clock (basic)
    ('1:0:10',  1*h + 0*m + 10*s,       'Full clock format'),
    ('1:20',    1*h + 20*m,             'Full clock format without seconds'),
    ('-:40',    40*m,                   'Only minutes in clock format'),
    (':50:30',  50*m + 30*s,            'Full clock format without hours'),

    # Clock (inline)
    ('1..30',   1*h + 30*m,             'Inline clock format'),
    ('1..5',    1*h + 5*m,              'Inline clock format without lead zero'),
    ('+..35',   35*m,                   'Plus and inline clock format in short'),
    (',,10',    10*m,                   'Inline clock with comas'),
    ('..,,10',  10*s,                   'Seconds in clock format without MM and HH with mixed separator'),
    ('-::40',   40*s,                   'Clock format, only seconds'),

    # Decimal errors
    ('1.2.3',   0,                      'Not allowed mixin decimal and clock formats (dots)'),
    ('-4,,5.6', 0,                      'Not allowed mixin decimal and clock formats (comas, partially both formats)'),
    ('7,8..9',  0,                      'Not allowed mixin decimal and clock formats (possible clock after digital)'),

    # Clock errors
    (':82',     0,                      'Not allowed more than 59 in clock format (inline)'),
    ('..82',    0,                      'Not allowed more than 59 in clock format (inline)'),
    ('....82',  0,                      'Not allowed more than 59 for seconds'),

    # Other errors
    ('-text',   0,                      'Not allowed text'),
    ('0-0',     0,                      'Not allowed symbols'),
    ('a0.0',    0,                      'Not allowed symbols in the beginning'),
    ('0-.0',    0,                      'Not allowed symbols in the middle'),
    ('0.0-',    0,                      'Not allowed symbols in the end'),
]


class TestTimeConverter(TestCase):
    def test_parse(self):
        self.line()
        print('TestTimeConverter')
        self.line()
        print(self.format('Input', 'Should be', 'Got', 'Description'))
        self.line()
        for data, should_be, descr in TEST_PARSER_TIME_DATA:
            got = parse_time(data)
            print(self.format(str(data), should_be, str(got), descr))
            self.assertEqual(got, should_be, 'In test "{}" got "{}"'.format(descr, got))
            self.assertIsInstance(got, type(should_be), 'Got {}({}), should be {}({})'.format(
                type(got).__name__,
                got,
                type(should_be).__name__,
                should_be
            ))

    @staticmethod
    def format(a, b, c, d):
        return '{:<14}{:<12}{:<14}{}'.format(a, b, c, d)

    @staticmethod
    def line():
        print('-' * 80)


if __name__ == '__main__':
    main()
