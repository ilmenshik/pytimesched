#!/bin/bash
set -e

export BUILD_VERSION="${1:-v0.0-dev}"
export DIR_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export DIR_BUILD="$DIR_ROOT/distr"
export DIR_LIB="$DIR_ROOT/venv/lib64/python3.10/site-packages/" # separator is ":"
export FILE_ARCHIVE="${2:-$DIR_BUILD/pyTimeSched-${BUILD_VERSION}.tar.gz}"

### MAIN BLOCK #########################################################

cd "$DIR_ROOT"
source ci/build_scripts/load_venv.sh
bash ci/build_scripts/bump_version.sh
bash ci/build_scripts/compile_onefile.sh
bash ci/build_scripts/archive_distr.sh
bash ci/build_scripts/banner.sh "BUILD SUCCESSFULLY\nDistr $FILE_ARCHIVE"
